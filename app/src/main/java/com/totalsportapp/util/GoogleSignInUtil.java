package com.totalsportapp.util;

import android.content.Context;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

public class GoogleSignInUtil {

    public static GoogleSignInClient getGoogleSignInClient(Context ctx){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("852810733812-guhu74g652vi72tl4ptbdq9jp9fgcl63.apps.googleusercontent.com")
                .requestEmail()
                .requestProfile()
                .build();

        return GoogleSignIn.getClient(ctx, gso);
    }

}
