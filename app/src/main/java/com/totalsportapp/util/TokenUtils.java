package com.totalsportapp.util;

import com.totalsportapp.data.api.model.TokenAPI;
import com.totalsportapp.data.prefs.PreferencesHelper;

import java.util.TimeZone;

import hirondelle.date4j.DateTime;

public class TokenUtils {

    public static void saveToken(TokenAPI tokenAPI){
        DateTime dtNow = DateTime.now(TimeZone.getDefault());

        int expireHalfMin = tokenAPI.getExpires() / 2;

        DateTime dtExpireAt = dtNow.plus(0, 0, 0, 0, expireHalfMin, 0, 0, DateTime.DayOverflow.Spillover);

        PreferencesHelper pref = PreferencesHelper.getInstance();
        pref.setAccessToken(tokenAPI.getAccessToken());
        pref.setRefreshToken(tokenAPI.getRefreshToken());
        pref.setExpireDateTime(dtExpireAt);
    }
}

