package com.totalsportapp.util;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.totalsportapp.R;

import java.util.Calendar;
import java.util.Locale;

import hirondelle.date4j.DateTime;

import static com.totalsportapp.util.AppConstants.DATETIME_FORMAT_API;
import static com.totalsportapp.util.AppConstants.DATETIME_FORMAT_CLIENT;
import static com.totalsportapp.util.AppConstants.DATE_FORMAT_CLIENT;

public class DateUtils {

    public static DateTime fromDateClient(String dateTimeText) {
        /*
        if(DATE_FORMAT_CLIENT != "DD/MM/YYYY"){
            throw new Exception("formato de fecha no configurado");
        }*/
        String day = dateTimeText.substring(0, 2);
        String month = dateTimeText.substring(3, 5);
        String year = dateTimeText.substring(6);

        return new DateTime(year + "-" + month + "-" + day);
    }

    /*public static DateTime fromDatetimeClient(String dateTimeText) {
        if(dateTimeText.length() != 19){
            return null;
        }

        String day = dateTimeText.substring(0, 2);
        String month = dateTimeText.substring(3, 5);
        String year = dateTimeText.substring(6, 10);

        String hour = dateTimeText.substring(11, 13);
        String minute = dateTimeText.substring(14, 16);
        String indicatorAMPM = dateTimeText.substring(17, 19);

        int iHour = Integer.parseInt(hour);

        if(indicatorAMPM == "PM" && (iHour > 1 && iHour <= 11)){
            iHour = Integer.parseInt(hour) + 12;
            hour = String.valueOf(iHour);
        }

        return new DateTime(year + "-" + month + "-" + day + " " + hour + ":" + minute);
    }*/

    public static void setDatePickerDialog(final Context ctx, final EditText etxt){
        final Calendar newCalendar = Calendar.getInstance();
        etxt.setFocusableInTouchMode(false);

        etxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        newCalendar.set(year, monthOfYear, dayOfMonth);
                        DateTime dateTime = new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0, 0, 0);
                        etxt.setText(dateTime.format(DATE_FORMAT_CLIENT));
                    }

                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    public static void setDatetimePickerDialog(final Context ctx, final EditText etxt){
        final Calendar newCalendar = Calendar.getInstance();
        etxt.setFocusableInTouchMode(false);

        etxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker view, final int year, final int monthOfYear,final int dayOfMonth) {
                        new TimePickerDialog(ctx, new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                newCalendar.set(year, monthOfYear, dayOfMonth);
                                newCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                newCalendar.set(Calendar.MINUTE, minute);

                                DateTime dateTime = new DateTime(year, monthOfYear + 1, dayOfMonth, hourOfDay, minute, 0, 0);
                                Locale spanish = new Locale("es", "ES");
                                etxt.setText(dateTime.format(DATETIME_FORMAT_CLIENT, spanish));
                                etxt.setTag(R.id.tag_etxt_datetime, dateTime.format(DATETIME_FORMAT_API));
                            }
                        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), false).show();
                    }
                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    public static DateTime getDatetime(EditText etxt){
        Object tag = etxt.getTag(R.id.tag_etxt_datetime);
        if(tag == null){
            return null;
        }
        return new DateTime(tag.toString());
    }
}

