package com.totalsportapp.util;

/**
 * Created by miguel on 15/03/17.
 */

public interface OnClickListListener {
    void onClick(int position);
}
