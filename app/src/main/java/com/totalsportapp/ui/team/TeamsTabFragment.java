package com.totalsportapp.ui.team;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;
import com.totalsportapp.MainApplication;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.db.model.DaoSession;
import com.totalsportapp.data.db.model.Equipo;
import com.totalsportapp.data.db.model.EquipoDao;
import com.totalsportapp.data.db.model.EquipoLugar;
import com.totalsportapp.data.db.model.EquipoLugarDao;
import com.totalsportapp.ui.BaseFragment;
import com.totalsportapp.ui.main.tabs.TeamAdapter;
import com.totalsportapp.ui.teamcalendar.TeamCalendar;
import com.totalsportapp.ui.teamcalendar.TeamCalendarActivity;
import com.totalsportapp.ui.teamcalendar.TeamCalendarActivityPrueba;
import com.totalsportapp.ui.teamprofile.TeamProfileActivity;
import com.totalsportapp.util.NetworkUtils;
import com.totalsportapp.util.ScrollChildSwipeRefreshLayout;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class TeamsTabFragment extends BaseFragment {

    //region controls
    private SpeedDialView speedDialView;
    private ListView lv_teams;
    private RelativeLayout rl_empty_teams;
    private RelativeLayout rl_loading_teams;
    //endregion

    public static final int CODE_INSERT_UPDATE_TEAM = 2390;

    public static TeamsTabFragment newInstance() {
        return new TeamsTabFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_equipos_tab, container, false);

        onCreateBase();

        findControls(layout);

        configureControls();

        return layout;
    }

    private void loadTeamsSync(){

        rl_loading_teams.setVisibility(View.VISIBLE);

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<Equipo>> call = authServicesAPI.myTeams();
        call.enqueue(new Callback<ArrayList<Equipo>>() {
            @Override
            public void onResponse(Call<ArrayList<Equipo>> call, Response<ArrayList<Equipo>> response) {
                if(response.isSuccessful()){
                    DaoSession daoSession = ((MainApplication) getActivity().getApplication()).getDaoSession();
                    EquipoDao equipoDao = daoSession.getEquipoDao();
                    EquipoLugarDao equipoLugarDao = daoSession.getEquipoLugarDao();
                    equipoLugarDao.deleteAll();
                    equipoDao.deleteAll();

                    ArrayList<Equipo> result = response.body();

                    for(Equipo item : result){
                        item.setId(null);
                        equipoDao.insert(item);
                        for(EquipoLugar itemEL : item.getLugares()){
                            itemEL.setId(null);
                            equipoLugarDao.insert(itemEL);
                        }
                    }
                }

                loadTeamsFromLocal();
            }

            @Override
            public void onFailure(Call<ArrayList<Equipo>> call, Throwable t) {

            }
        });
    }

    public void loadTeamsFromLocal(){
        ArrayList<Equipo> equipos = new ArrayList<>();
        equipos.addAll(((MainApplication)getActivity().getApplication()).getDaoSession().getEquipoDao().loadAll());

        if(equipos.size() > 0){
            TeamAdapter teamAdapter = new TeamAdapter(ctx, equipos);
            lv_teams.setAdapter(teamAdapter);
            lv_teams.setVisibility(View.VISIBLE);
            rl_empty_teams.setVisibility(View.GONE);
            lv_teams.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView txtv_my_team_icon = view.findViewById(R.id.txtv_my_team_icon);

                    if(txtv_my_team_icon.getVisibility() == View.VISIBLE){
                        Intent i = new Intent(ctx, CreateTeamActivity.class);
                        int idEquipo = (int) view.getTag();
                        i.putExtra(CreateTeamActivity.KEY_TEAM_DATA, idEquipo);
                        startActivityForResult(i, CODE_INSERT_UPDATE_TEAM);
                    } else {
                        Intent i = new Intent(ctx, TeamProfileActivity.class);
                        int idEquipo = (int) view.getTag();
                        i.putExtra(TeamProfileActivity.KEY_TEAM_ID, idEquipo);
                        i.putExtra(TeamProfileActivity.KEY_TYPE_TEAM_PROFILE, TeamProfileActivity.TYPE_PROFILE);
                        startActivity(i);
                    }
                }
            });
        }else{
            lv_teams.setVisibility(View.GONE);
            rl_empty_teams.setVisibility(View.VISIBLE);
        }

        if(rl_loading_teams.getVisibility() == View.VISIBLE){
            rl_loading_teams.setVisibility(View.GONE);
        }
    }

    private void findControls(View layout){
        speedDialView = layout.findViewById(R.id.speedDial);
        lv_teams = layout.findViewById(R.id.lv_teams);
        rl_empty_teams = layout.findViewById(R.id.rl_empty_teams);
        rl_loading_teams = layout.findViewById(R.id.rl_loading_teams);

    }

    private void configureControls(){
        setUpSpeedDialView();

        if(NetworkUtils.isNetworkConnected(ctx)){
            loadTeamsSync();
        }else{
            loadTeamsFromLocal();
        }
    }

    private void setUpSpeedDialView(){
        speedDialView.addActionItem(
                new SpeedDialActionItem.Builder(R.id.fab_crear_equipo, R.drawable.team_create_icon)
                        .setLabel("Crear un equipo")
                        .setLabelColor(Color.WHITE)
                        .setLabelClickable(false)
                        .setLabelBackgroundColor(Color.TRANSPARENT)
                        .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.fab_equipos_icons,
                                ctx.getTheme()))
                        .create()
        );

        speedDialView.addActionItem(
                new SpeedDialActionItem.Builder(R.id.fab_calendario, R.drawable.calendar_today_white_24dp)
                        .setLabel("Ver Calendario")
                        .setLabelColor(Color.WHITE)
                        .setLabelClickable(false)
                        .setLabelBackgroundColor(Color.TRANSPARENT)
                        .setFabBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.fab_equipos_icons,
                                ctx.getTheme()))
                        .create()
        );

        speedDialView.setOnActionSelectedListener(new SpeedDialView.OnActionSelectedListener() {
            @Override
            public boolean onActionSelected(SpeedDialActionItem actionItem) {
                if(actionItem.getId() == R.id.fab_crear_equipo){
                    Intent i = new Intent(ctx, CreateTeamActivity.class);
                    startActivityForResult(i, CODE_INSERT_UPDATE_TEAM);
                } else if(actionItem.getId() == R.id.fab_calendario){
                    Intent i = new Intent(ctx, TeamCalendar.class);
                    startActivity(i);
                }
                return false;
            }
        });

        speedDialView.setMainFabOpenedBackgroundColor(ContextCompat.getColor(ctx, R.color.fab_equipos_expand));
        speedDialView.setMainFabClosedBackgroundColor(ContextCompat.getColor(ctx, R.color.fab_equipos));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_INSERT_UPDATE_TEAM && resultCode == RESULT_OK) {
            if(NetworkUtils.isNetworkConnected(ctx)){
                loadTeamsSync();
            }else{
                loadTeamsFromLocal();
            }
        }
    }

}
