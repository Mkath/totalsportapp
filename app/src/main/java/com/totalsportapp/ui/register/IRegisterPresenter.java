package com.totalsportapp.ui.register;

import com.totalsportapp.data.api.model.RegisterRequest;

public interface IRegisterPresenter {

    void Register(RegisterRequest registerRequest);
}
