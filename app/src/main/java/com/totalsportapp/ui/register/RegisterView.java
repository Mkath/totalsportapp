package com.totalsportapp.ui.register;

import java.util.List;

public interface RegisterView {

    //void saveAuthToken(String accessToken, String refreshToken);

    void onResultRegisterSuccess(boolean isLogged);
    void showResultRegisterFail(List<String> errors);

    void showFormValidateError(String messague);
    void showProcessingRegister();

    void showInternalError();
}
