package com.totalsportapp.ui.main;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.totalsportapp.MainApplication;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.db.model.Usuario;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.login.LoginActivity;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseAppCompatActivity implements MainView {

    private IMainPresenter presenter;

    //region controls
    private ViewPager viewPager;
    private TabLayout tabLayout;
    //endregion
    private LocationManager locationManager;


    public final static String KEY_TAB_LOAD = "KEY_TAB_LOAD";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        onCreateBase();

        findControls();

        setUpTabs();

        requestLocationPermision();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int tabSelected = extras.getInt(KEY_TAB_LOAD, 0);
            if (tabSelected != 0) {
                viewPager.setCurrentItem(tabSelected);
            }
        }

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if(PreferencesHelper.getInstance().getUbicationKey()){
            checkLocation();
            PreferencesHelper.getInstance().setUbicationKey(false);
        }

        presenter = new MainPresenter(this, ((MainApplication) getApplication()).getDaoSession());
        presenter.syncUserApp();
        if (getPrefSyncUser() == false) {
            presenter.syncUbigeo();
            presenter.syncDepartamento();
            presenter.syncProvincia();
        }

        final PreferencesHelper pref = PreferencesHelper.getInstance();

        if (pref.getTokenFCM() == null) {
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                return;
                            }

                            final String token = task.getResult().getToken();

                            AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
                            Call<ResponseBody> call = authServicesAPI.saveTokenFCM(token);
                            call.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if (response.isSuccessful()) {
                                        pref.setTokenFCM(token);
                                    } else {
                                        pref.setTokenFCM(null);
                                    }
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    pref.setTokenFCM(null);
                                }
                            });
                        }
                    });
        }

    }

    private void findControls() {
        viewPager = findViewById(R.id.viewpager);
        tabLayout = findViewById(R.id.tab_layout);
    }

    private void setUpTabs() {
        viewPager.setAdapter(new TabFragmentPagerAdapter(getSupportFragmentManager(),
                MainActivity.this));

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                invalidateOptionsMenu();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setPrefSyncUser(false);
    }

    private boolean checkLocation() {

        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Activar ubicación")
                .setMessage("Su ubicación esta desactivada.\npor favor active su ubicación ")
                .setCancelable(false)
                .setPositiveButton("Configuración de ubicación", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Toast.makeText(ctx, "Si no enciende su ubicación, no podrá ver la unibicación de sus compañeros", Toast.LENGTH_SHORT).show();
                    }
                });
        dialog.show();

    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    public boolean getPrefSyncUser() {
        return PreferencesHelper.getInstance().getSyncUser();
    }

    @Override
    public void setPrefSyncUser(boolean syncUser) {
        PreferencesHelper.getInstance().setSyncUser(syncUser);
    }

    @Override
    public void setPrefUser(Usuario usuario) {
        PreferencesHelper.getInstance().setUser(usuario);
    }

    @Override
    public void onErrorSyncProfile() {
        Toast.makeText(ctx, R.string.error_sync_user, Toast.LENGTH_LONG).show();

        Intent i = new Intent(ctx, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);

        overridePendingTransition(R.anim.anim_enter, R.anim.anim_leave);
    }

    @Override
    public void onErrorSyncUbigeo() {
        Toast.makeText(ctx, R.string.internal_error, Toast.LENGTH_LONG).show();

        Intent i = new Intent(ctx, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);

        overridePendingTransition(R.anim.anim_enter, R.anim.anim_leave);
    }
}
