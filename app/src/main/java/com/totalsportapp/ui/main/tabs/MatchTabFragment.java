package com.totalsportapp.ui.main.tabs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.totalsportapp.R;
import com.totalsportapp.ui.jointeam.JoinTeamActivity;
import com.totalsportapp.ui.challengeteam.ChallengeTeamActivity;

public class MatchTabFragment extends Fragment {

    private Context ctx;

    public MatchTabFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_match_tab, container, false);

        ctx = getContext();

        Button btn_unirme = layout.findViewById(R.id.btn_unirme);
        btn_unirme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ctx, JoinTeamActivity.class);
                ctx.startActivity(i);
            }
        });

        Button btn_retar = layout.findViewById(R.id.btn_retar);
        btn_retar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ctx, ChallengeTeamActivity.class);
                ctx.startActivity(i);
            }
        });

        return layout;
    }
}
