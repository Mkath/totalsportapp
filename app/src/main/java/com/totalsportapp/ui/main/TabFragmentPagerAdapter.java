package com.totalsportapp.ui.main;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import com.totalsportapp.R;
import com.totalsportapp.ui.team.TeamsTabFragment;
import com.totalsportapp.ui.main.tabs.MatchTabFragment;
import com.totalsportapp.ui.main.tabs.ProfileTabFragment;
import com.totalsportapp.ui.teamcalendar.TeamCalendarActivity;
import com.totalsportapp.ui.teamcalendar.TeamCalendarActivityPrueba;

public class TabFragmentPagerAdapter extends FragmentPagerAdapter {

    final int PAGE_COUNT = 3;
    private String tabTitles[] = new String[] { "Pichanguea", "Calendario", "Mi Perfil" };
    private int tabIcons[] = new int[]{R.drawable.match_icon_tab, R.drawable.team_icon_tab, R.drawable.profile_icon_tab };
    private Context context;

    public TabFragmentPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position)
        {
            case 0:
                return new MatchTabFragment();
            case 1:
                return new TeamCalendarActivityPrueba();
            case 2:
                return new ProfileTabFragment();
        }
        return new MatchTabFragment();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Drawable image = context.getResources().getDrawable(tabIcons[position]);
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());

        SpannableString sb = new SpannableString("   " + tabTitles[position]);
        ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;
    }

}
