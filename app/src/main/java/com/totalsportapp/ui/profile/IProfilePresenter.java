package com.totalsportapp.ui.profile;

import com.totalsportapp.data.db.model.Usuario;

import java.util.List;

public interface IProfilePresenter {

    void saveProfile(Usuario usuario);

}
