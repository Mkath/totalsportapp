package com.totalsportapp.ui.profile;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.EquipoMiembroAPI;
import com.totalsportapp.data.db.model.EquipoMiembro;
import com.totalsportapp.data.db.model.Gender;
import com.totalsportapp.data.db.model.Usuario;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.login.LoginActivity;
import com.totalsportapp.util.DateUtils;
import com.totalsportapp.util.FileAppUtils;
import com.totalsportapp.util.NetworkUtils;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.totalsportapp.util.AppConstants.BASE_URL;
import static com.totalsportapp.util.AppConstants.BASE_URL_PHOTO;
import static com.totalsportapp.util.AppConstants.DATE_FORMAT_CLIENT;

public class ProfileActivity extends BaseAppCompatActivity implements View.OnClickListener, ProfileView {

    private IProfilePresenter presenter;

    //region controls

    private Toolbar toolbar;
    private Button btnSaveProfile;

    private EditText etxtFirstname;
    private EditText etxtLastname;
    //private EditText etxtEmail;
    private EditText etxt_position;
    private EditText etxtNickname;
    private EditText etxtBirthdate;
    private EditText etxtEmergencyNumber;
    private RadioButton rbMan;
    private RadioButton rbWoman;
    private ImageView imgvProfilePhoto;

    private EquipoMiembroAPI amigoAPI = null;

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        presenter = new ProfilePresenter(this);

        onCreateBase();

        findControls();

        configureControls();

        if(getIntent().getExtras()!=null){
            amigoAPI = (EquipoMiembroAPI) getIntent().getExtras().getSerializable("amigoAPI");
        }

        if(NetworkUtils.isNetworkConnected(ctx)) {
            if(amigoAPI!=null){
                readUsuarioAPI(amigoAPI);
                setUpToolbarProfile();
                UIEnabled(false);
                btnSaveProfile.setVisibility(View.GONE);
            }else{
                setUpToolbar();
                readUsuario();
                UIEnabled(true);
                btnSaveProfile.setVisibility(View.VISIBLE);
            }
        }else {
            showDisconnectedNetwork();
            onBackPressed();
        }

    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        etxtFirstname = findViewById(R.id.etxt_firstname);
        etxtLastname = findViewById(R.id.etxt_lastname);
        etxtNickname = findViewById(R.id.etxt_nickname);
        etxt_position = findViewById(R.id.etxt_position);
        etxtBirthdate = findViewById(R.id.etxt_birthdate);
        etxtEmergencyNumber = findViewById(R.id.etxt_emergency_number);
        rbMan = findViewById(R.id.rb_man);
        rbWoman = findViewById(R.id.rb_woman);
        imgvProfilePhoto = findViewById(R.id.imgv_profile_photo);
        btnSaveProfile = findViewById(R.id.btn_save_profile);
    }

    private void configureControls(){

        btnSaveProfile.setOnClickListener(this);
        btnSaveProfile.setEnabled(true);
        DateUtils.setDatePickerDialog(ctx, etxtBirthdate);
        imgvProfilePhoto.setClipToOutline(true);
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Editar Perfil");
    }

    private void setUpToolbarProfile(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Perfil");
    }

    private void readUsuario(){
        Usuario usuario = PreferencesHelper.getInstance().getUser();

        etxtFirstname.setText(usuario.getNombres());
        etxtLastname.setText(usuario.getApellidos());
        etxtNickname.setText(usuario.getAlias());
        etxt_position.setText(usuario.getPosicion());

        if(usuario.getFechaNacimiento() != null){
            etxtBirthdate.setText(usuario.getFechaNacimiento().format(DATE_FORMAT_CLIENT));
        }
        etxtEmergencyNumber.setText(usuario.getContactoEmergencia());

        if(usuario.getSexo() != null){
            if(usuario.getSexo() == Gender.MAN){
                rbMan.setChecked(true);
            }else if(usuario.getSexo() == Gender.WOMAN) {
                rbWoman.setChecked(true);
            }
        }

        if(usuario.getFoto() != null){
            imgvProfilePhoto.setPadding(0,0,0,0);
            String photoUrl = usuario.getFoto();
            //have to build project if GlideApp is not found
            Glide.with(this)
                    .load(photoUrl)
                    .dontAnimate()
                    .centerCrop()
                    .into(imgvProfilePhoto);
        }
    }
    private void readUsuarioAPI(EquipoMiembroAPI amigoAPI){

        etxtFirstname.setText(amigoAPI.getNombres());
        etxtLastname.setText(amigoAPI.getApellidos());
        etxtNickname.setText(amigoAPI.getAlias());


       // etxt_position.setText(amigoAPI.getPosicion());

       /* if(amigoAPI.getFechaNacimiento() != null){
            etxtBirthdate.setText(amigoAPI.getFechaNacimiento().format(DATE_FORMAT_CLIENT));
        }
        etxtEmergencyNumber.setText(amigoAPI.getContactoEmergencia());

        if(amigoAPI.getSexo() != null){
            if(amigoAPI.getSexo() == Gender.MAN){
                rbMan.setChecked(true);
            }else if(amigoAPI.getSexo() == Gender.WOMAN) {
                rbWoman.setChecked(true);
            }
        }

        if(amigoAPI.getFoto() != null){
            imgvProfilePhoto.setPadding(0,0,0,0);
            String photoUrl = BASE_URL_PHOTO + amigoAPI.getFoto();
            //have to build project if GlideApp is not found
            Glide.with(this)
                    .load(photoUrl)
                    .dontAnimate()
                    .centerCrop()
                    .into(imgvProfilePhoto);
        }*/
    }

    public void UIEnabled(boolean isEnabled){
        etxtFirstname.setEnabled(isEnabled);
        etxtLastname.setEnabled(isEnabled);
        etxtNickname.setEnabled(isEnabled);
        etxt_position.setEnabled(isEnabled);
        etxtBirthdate.setEnabled(isEnabled);
        etxtEmergencyNumber.setEnabled(isEnabled);
        rbMan.setEnabled(isEnabled);
        rbWoman.setEnabled(isEnabled);
        imgvProfilePhoto.setEnabled(isEnabled);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveProfileClick(){
        Usuario usuario = PreferencesHelper.getInstance().getUser();
        usuario.setNombres(etxtFirstname.getText().toString());
        usuario.setApellidos(etxtLastname.getText().toString());
        usuario.setAlias(etxtNickname.getText().toString());
        usuario.setPosicion(etxt_position.getText().toString());
        if(etxtBirthdate.getText().toString().isEmpty()){
            usuario.setFechaNacimiento(null);
        } else {
            usuario.setFechaNacimiento(DateUtils.fromDateClient(etxtBirthdate.getText().toString()));
        }
        usuario.setContactoEmergencia(etxtEmergencyNumber.getText().toString());
        if(rbWoman.isChecked()){
            usuario.setSexo(Gender.WOMAN);
        }else if(rbMan.isChecked()){
            usuario.setSexo(Gender.MAN);
        }

        presenter.saveProfile(usuario);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_save_profile:
                saveProfileClick();
                break;
        }
    }

    @Override
    public void setPrefUser(Usuario usuario) {
        PreferencesHelper.getInstance().setUser(usuario);
    }

    @Override
    public void onResultSaveProfileSuccess() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        Toast.makeText(ctx, "¡Perfil Actualizado!", Toast.LENGTH_LONG).show();

        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void showResultSaveProfileFail(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert(errors.get(0));
    }

    @Override
    public void showFormValidateError(String messague) {
        simpleAlert(messague);
    }

    @Override
    public void showProcessingSaveProfile() {
        progressDialog.show();
    }

    @Override
    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }
}
