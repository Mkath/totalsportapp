package com.totalsportapp.ui.jointeam;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.EquipoSolicitudUnirRequest;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.ui.BaseAppCompatActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JoinTeamFormActivity extends BaseAppCompatActivity {

    public static final String KEY_TEAM_ID = "KEY_TEAM_ID";
    public static final String KEY_TEAM_NAME = "KEY_TEAM_NAME";

    //region controls
    private Toolbar toolbar;
    private TextView txtv_team_join;
    private EditText etxt_commentaries;
    private Button btn_send_request_join;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_team_form);

        onCreateBase();

        findControls();

        configureControls();

        String teamName = getIntent().getExtras().getString(KEY_TEAM_NAME);
        txtv_team_join.setText("Unirme al equipo:\n" + teamName);
    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        txtv_team_join = findViewById(R.id.txtv_team_join);
        etxt_commentaries = findViewById(R.id.etxt_commentaries);
        btn_send_request_join = findViewById(R.id.btn_send_request_join);
    }

    private void configureControls(){
        setUpToolbar();

        btn_send_request_join.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_send_request_join_click();
            }
        });
    }

    private void btn_send_request_join_click(){

        int idEquipo = getIntent().getExtras().getInt(KEY_TEAM_ID);
        String comments = etxt_commentaries.getText().toString();

        if(comments.length() > 400){
            simpleAlert("Máximo 400 carácteres para sus comentarios");
            return;
        }

        progressDialog.show();

        EquipoSolicitudUnirRequest equipoSolicitudUnirRequest = new EquipoSolicitudUnirRequest();
        equipoSolicitudUnirRequest.setIdEquipo(idEquipo);
        equipoSolicitudUnirRequest.setComentariosSolicitud(comments);

        AuthServicesAPI servicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<OperationResult> call = servicesAPI.joinToTeam(equipoSolicitudUnirRequest);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if(response.isSuccessful()){
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }

                    OperationResult operationResult = response.body();

                    if(operationResult.isStatus()){
                        Toast.makeText(ctx, "¡Solicitud Enviada!", Toast.LENGTH_LONG).show();
                        finish();
                    }else{
                        showResultRequestJoinFail(operationResult.getErrors());
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                showInternalError();
            }
        });
    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    public void showResultRequestJoinFail(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("Error", errors.get(0));
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Unirme");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
