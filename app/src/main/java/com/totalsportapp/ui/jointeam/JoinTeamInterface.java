package com.totalsportapp.ui.jointeam;

import com.totalsportapp.data.db.model.Equipo;

/**
 * Created by miguel on 15/03/17.
 */

public interface JoinTeamInterface {
    void onClick(Equipo equipo);
}
