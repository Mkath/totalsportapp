package com.totalsportapp.ui.requestchallenge;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.EquipoMiembroAPI;
import com.totalsportapp.data.api.model.InviteMemberModel;
import com.totalsportapp.data.api.model.MiembroParticipante;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.BaseFragment;
import com.totalsportapp.util.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class TeamMembersRequestFragment extends BaseFragment implements MemberRequestInterface{

    //region controls
    private ListView lv_teams;
    private RelativeLayout rl_empty_teams;
    private RelativeLayout rl_loading_teams;
    private int idEquipo;
    private int idReto;
    private  ArrayList<MiembroParticipante> equipos;
    //endregion

    public static final int CODE_INSERT_UPDATE_TEAM = 2390;
    public final static String KEY_TEAM_ID = "KEY_TEAM_ID";
    public final static String KEY_CHALLENGE_ID = "KEY_CHALLENGE_ID";

    public static TeamMembersRequestFragment newInstance(Bundle bundle) {
        TeamMembersRequestFragment teamMembersRequestFragment = new TeamMembersRequestFragment();
        teamMembersRequestFragment.setArguments(bundle);
        return  teamMembersRequestFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_equipos_request, container, false);

        onCreateBase();

        findControls(layout);

        configureControls();

        return layout;
    }


  /*  private void loadTeamsSync(){

        rl_loading_teams.setVisibility(View.VISIBLE);

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<Equipo>> call = authServicesAPI.teamMembers();
        call.enqueue(new Callback<ArrayList<Equipo>>() {
            @Override
            public void onResponse(Call<ArrayList<Equipo>> call, Response<ArrayList<Equipo>> response) {
                if(response.isSuccessful()){
                    DaoSession daoSession = ((MainApplication) getActivity().getApplication()).getDaoSession();
                    EquipoDao equipoDao = daoSession.getEquipoDao();
                    EquipoLugarDao equipoLugarDao = daoSession.getEquipoLugarDao();
                    equipoLugarDao.deleteAll();
                    equipoDao.deleteAll();

                    ArrayList<Equipo> result = response.body();

                    for(Equipo item : result){
                        item.setId(null);
                        equipoDao.insert(item);
                        for(EquipoLugar itemEL : item.getLugares()){
                            itemEL.setId(null);
                            equipoLugarDao.insert(itemEL);
                        }
                    }
                }

                loadTeamsFromLocal();
            }

            @Override
            public void onFailure(Call<ArrayList<Equipo>> call, Throwable t) {

            }
        });
    }*/

    private void displayMembers(){
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("Espere por favor...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<MiembroParticipante>> call = services.teamMembersByReto(idEquipo, idReto);
        call.enqueue(new Callback<ArrayList<MiembroParticipante>>() {
            @Override
            public void onResponse(Call<ArrayList<MiembroParticipante>> call, Response<ArrayList<MiembroParticipante>> response) {
                if(response.isSuccessful()){
                    equipos = response.body();
                    ArrayList<MiembroParticipante> newList = new ArrayList<>();

                    for (int i = 0; i <equipos.size() ; i++) {
                        if(equipos.get(i).getEsCapitan()!=1){
                            if(equipos.get(i).getEstado()!=1){
                                newList.add(equipos.get(i));
                            }
                        }
                    }
                   getAdapter(newList);

                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MiembroParticipante>> call, Throwable t) {
                showInternalError();
            }
        });
    }


    private void showInternalError(){
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        Toast.makeText(ctx, R.string.internal_error, Toast.LENGTH_LONG).show();
    }


    private void getAdapter( ArrayList<MiembroParticipante> equipos ){

        MemberRequestAdapter memberRequestAdapter = new MemberRequestAdapter(ctx, equipos, this , idEquipo);
        lv_teams.setAdapter(memberRequestAdapter);
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
    }

   /* public void loadTeamsFromLocal(){
        ArrayList<Equipo> equipos = new ArrayList<>();
        equipos.addAll(((MainApplication)getActivity().getApplication()).getDaoSession().getEquipoDao().loadAll());

        if(equipos.size() > 0){
            TeamAdapter teamAdapter = new TeamAdapter(ctx, equipos);
            lv_teams.setAdapter(teamAdapter);
            lv_teams.setVisibility(View.VISIBLE);
            rl_empty_teams.setVisibility(View.GONE);
            lv_teams.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView txtv_my_team_icon = view.findViewById(R.id.txtv_my_team_icon);

                    if(txtv_my_team_icon.getVisibility() == View.VISIBLE){
                        Intent i = new Intent(ctx, CreateTeamActivity.class);
                        int idEquipo = (int) view.getTag();
                        i.putExtra(CreateTeamActivity.KEY_TEAM_DATA, idEquipo);
                        startActivityForResult(i, CODE_INSERT_UPDATE_TEAM);
                    } else {
                        Intent i = new Intent(ctx, TeamProfileActivity.class);
                        int idEquipo = (int) view.getTag();
                        i.putExtra(TeamProfileActivity.KEY_TEAM_ID, idEquipo);
                        i.putExtra(TeamProfileActivity.KEY_TYPE_TEAM_PROFILE, TeamProfileActivity.TYPE_PROFILE);
                        startActivity(i);
                    }
                }
            });
        }else{
            lv_teams.setVisibility(View.GONE);
            rl_empty_teams.setVisibility(View.VISIBLE);
        }

        if(rl_loading_teams.getVisibility() == View.VISIBLE){
            rl_loading_teams.setVisibility(View.GONE);
        }
    }*/

    private void findControls(View layout){
        lv_teams = layout.findViewById(R.id.lv_teams);
        rl_empty_teams = layout.findViewById(R.id.rl_empty_teams);
        rl_loading_teams = layout.findViewById(R.id.rl_loading_teams);
        idEquipo = getArguments().getInt(KEY_TEAM_ID);
        idReto = getArguments().getInt(KEY_CHALLENGE_ID);

    }

    private void configureControls(){

        equipos = new ArrayList<>();
        if(NetworkUtils.isNetworkConnected(ctx)){
            displayMembers();
        }else{
            displayMembers();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CODE_INSERT_UPDATE_TEAM && resultCode == RESULT_OK) {
            if(NetworkUtils.isNetworkConnected(ctx)){
                displayMembers();
            }else{
                displayMembers();
            }
        }
    }

    @Override
    public void clickAdd(final int idAmigo) {

        final InviteMemberModel inviteMemberModel = new InviteMemberModel();
        inviteMemberModel.setIdEquipoMiembro(idAmigo);
        inviteMemberModel.setIdEquipoSolicitudRetar(idReto);
        confirmAlert("¿Desea invitar a su amigo?", new BaseAppCompatActivity.ExecuteParam() {
            @Override
            public void onExecute() {
                progressDialog.show();

                AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
                Call<OperationResult> call = services.challengeInviteMember(inviteMemberModel);
                call.enqueue(new Callback<OperationResult>() {
                    @Override
                    public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                        if (response.isSuccessful()) {
                            OperationResult opResult = response.body();
                            if(opResult.isStatus()){

                                if(response.code() == 200){
                                    Toast.makeText(ctx, "Invitación Enviada", Toast.LENGTH_LONG).show();
                                    displayMembers();

                                }
                                if(response.code() == 202){
                                    Toast.makeText(ctx, opResult.getErrors().get(0), Toast.LENGTH_LONG).show();
                                }
                                //lv_teams.setVisibility(View.GONE);
                               //displayMembers();
                                // displayMyFriends();
                                //setList(requestAmigos,amigos);
                            }else{
                                showResultInviteMember(opResult.getErrors());
                            }
                        }else{
                            showInternalError();
                        }
                    }

                    @Override
                    public void onFailure(Call<OperationResult> call, Throwable t) {
                        showInternalError();
                    }
                });
            }
        });
    }

    public void showResultInviteMember(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("Error", errors.get(0));
    }

    protected void confirmAlert(String messague, final BaseAppCompatActivity.ExecuteParam onOk){
        AlertDialog dialogLogout = new AlertDialog.Builder(ctx).create();
        dialogLogout.setMessage(messague);
        dialogLogout.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(onOk != null){
                            onOk.onExecute();
                        }
                    }
                });
        dialogLogout.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
        dialogLogout.show();
    }

    @Override
    public void clickAccept(int idSolicitud) {

    }

    @Override
    public void clickDeny(int idSolicitud) {

    }
}
