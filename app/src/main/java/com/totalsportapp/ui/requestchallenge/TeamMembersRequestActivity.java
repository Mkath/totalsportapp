package com.totalsportapp.ui.requestchallenge;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.totalsportapp.R;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.util.ActivityUtils;

public class TeamMembersRequestActivity extends BaseAppCompatActivity {


    private TeamMembersRequestFragment fragment;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back);

        toolbar = findViewById(R.id.toolbar);

        toolbar.setTitle("Invitar Miembros");

        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);

        fragment = (TeamMembersRequestFragment) getSupportFragmentManager()
                .findFragmentById(R.id.body);

        if (fragment == null) {
            fragment = TeamMembersRequestFragment.newInstance(getIntent().getExtras());

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.body);
        }

        // Create the presenter
        //new NewQuestionPresenter(fragment, this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
