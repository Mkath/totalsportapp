package com.totalsportapp.ui.requestchallenge;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.totalsportapp.R;
import com.totalsportapp.data.api.model.AmigoAPI;
import com.totalsportapp.data.api.model.EquipoMiembroAPI;
import com.totalsportapp.data.api.model.MiembroParticipante;
import com.totalsportapp.data.db.model.EstadoAmigo;
import com.totalsportapp.data.db.model.EstadoInvitacion;
import com.totalsportapp.ui.myfriends.SearchFriendInterface;
import com.totalsportapp.util.GlideApp;

import java.util.ArrayList;

public class MemberRequestAdapter extends ArrayAdapter<MiembroParticipante> {
    MemberRequestInterface memberRequestInterface;
    private int idEquipo;
    public MemberRequestAdapter(Context context, ArrayList<MiembroParticipante> items,
                                MemberRequestInterface memberRequestInterface, int idEquipo ) {
        super(context, 0, items);
        this.memberRequestInterface = memberRequestInterface;
        this.idEquipo = idEquipo;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_item_member_request, parent, false);
        }

        final MiembroParticipante amigoAPI = getItem(position);
        ImageView imgv_friend_photo = convertView.findViewById(R.id.imgv_friend_photo);
        TextView txtv_friend_name = convertView.findViewById(R.id.txtv_friend_name);
        TextView txtv_friend_nickname = convertView.findViewById(R.id.txtv_friend_nickname);
        TextView txtv_pend_friend = convertView.findViewById(R.id.txtv_pend_friend);
        TextView txtv_deny_friend = convertView.findViewById(R.id.txtv_deny_friend);
        RelativeLayout container_request = convertView.findViewById(R.id.rl_container_request);
        Button btn_accept_request = convertView.findViewById(R.id.btn_accept_request);
        Button btn_deny_request = convertView.findViewById(R.id.btn_deny_request);
        Button btn_add_friend = convertView.findViewById(R.id.btn_add_friend);
        RelativeLayout container = convertView.findViewById(R.id.general_container);

        if(amigoAPI.getFoto() != null){
            imgv_friend_photo.setPadding(0,0,0,0);
            String photoUrl =  amigoAPI.getFoto();
            //have to build project if GlideApp is not found
            GlideApp.with(getContext())
                    .load(photoUrl)
                    .dontAnimate()
                    .centerCrop()
                    .into(imgv_friend_photo);
        }else{

            GlideApp.with(getContext())
                    .load(getContext().getResources().getDrawable(R.drawable.logo_total_sport_png_512_x_512))
                    .into(imgv_friend_photo);
        }

        txtv_friend_name.setText(amigoAPI.getUsuarioNombre()+ " " + amigoAPI.getUsuarioApellido());

        if(amigoAPI.getAlias() != null){
            txtv_friend_nickname.setText(amigoAPI.getAlias());
        }else{
            txtv_friend_nickname.setText("Sin alias");
        }

        container_request.setVisibility(View.GONE);
        txtv_pend_friend.setVisibility(View.GONE);
        txtv_deny_friend.setVisibility(View.GONE);

        btn_add_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                memberRequestInterface.clickAdd(amigoAPI.getIdEquipoMiembro());
            }
        });

        if(amigoAPI.getEstado() == EstadoInvitacion.INVITADO ){
            txtv_pend_friend.setVisibility(View.VISIBLE);
            btn_add_friend.setVisibility(View.GONE);

        } else if( amigoAPI.getEstado() == EstadoInvitacion.DENEGADO){
            txtv_deny_friend.setVisibility(View.VISIBLE);
            btn_add_friend.setVisibility(View.GONE);

        }else if( amigoAPI.getEstado() == EstadoInvitacion.NO_INVITADO){
            txtv_pend_friend.setVisibility(View.GONE);
            txtv_deny_friend.setVisibility(View.GONE);
            btn_add_friend.setVisibility(View.VISIBLE);}

       /* if(amigoAPI.()){
            container_request.setVisibility(View.GONE);
            if(amigoAPI.getEstado() == EstadoAmigo.PENDIENTE ||
                    (amigoAPI.getEstado() == EstadoAmigo.DENEGADO && amigoAPI.isAgregadoPorMi() == true)){
                txtv_pend_friend.setVisibility(View.VISIBLE);
            } else if( amigoAPI.getEstado() == EstadoAmigo.DENEGADO && amigoAPI.isAgregadoPorMi() == false){
                txtv_deny_friend.setVisibility(View.VISIBLE);
            }
        }else {
            if(amigoAPI.getEstado() == EstadoAmigo.ACEPTADO){
                container_request.setVisibility(View.GONE);
                txtv_pend_friend.setVisibility(View.GONE);
                txtv_deny_friend.setVisibility(View.GONE);
            }else{
                container_request.setVisibility(View.VISIBLE);
                txtv_pend_friend.setVisibility(View.GONE);
                txtv_deny_friend.setVisibility(View.GONE);
                btn_accept_request.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        friendInterface.clickAccept(amigoAPI.getIdSolicitud());
                    }
                });

                btn_deny_request.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        friendInterface.clickDeny(amigoAPI.getIdSolicitud());
                    }
                });
            }
        }*/

        return convertView;
    }
}
