package com.totalsportapp.ui.requestchallenge;

public interface MemberRequestInterface {
    void clickAdd(int idAmigo);

    void clickAccept(int idSolicitud);
    void clickDeny(int idSolicitud);
}
