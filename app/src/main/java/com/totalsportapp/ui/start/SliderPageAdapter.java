package com.totalsportapp.ui.start;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class SliderPageAdapter extends FragmentPagerAdapter {

    public static final int NUM_ITEMS = 3;

    public SliderPageAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        return SliderFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
}
