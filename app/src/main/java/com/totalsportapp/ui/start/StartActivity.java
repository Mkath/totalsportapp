package com.totalsportapp.ui.start;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.totalsportapp.R;
import com.totalsportapp.data.db.model.LoggedMode;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.login.LoginActivity;
import com.totalsportapp.ui.main.MainActivity;
import com.totalsportapp.ui.register.RegisterActivity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class StartActivity extends BaseAppCompatActivity {

    private int currentPageSlider = 0;
    private Timer timer;

    //region controls
    private ViewPager vpSlider;
    private Button btnGoLogin;
    private Button btnGoCreateAccount;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        onCreateBase();

        if(PreferencesHelper.getInstance().getCurrentUserLoggedInMode() == LoggedMode.LOGGED_IN){
            Intent i = new Intent(ctx, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

            overridePendingTransition(R.anim.anim_enter, R.anim.anim_leave);
        }

        findControls();

        configureControls();
    }

    private void findControls(){
        vpSlider = findViewById(R.id.vp_slider);
        btnGoLogin = findViewById(R.id.btn_go_login);
        btnGoCreateAccount = findViewById(R.id.btn_go_create_account);
    }

    private void configureControls(){
        FragmentPagerAdapter adapterViewPager = new SliderPageAdapter(getSupportFragmentManager());
        vpSlider.setAdapter(adapterViewPager);

        btnGoLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ctx, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

                overridePendingTransition(R.anim.anim_enter, R.anim.anim_leave);
            }
        });

        btnGoCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ctx, RegisterActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);

                overridePendingTransition(R.anim.anim_enter, R.anim.anim_leave);
            }
        });
    }

    @Override
    protected void onPause() {
        if(timer != null){
            timer.cancel();
            timer.purge();
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (currentPageSlider == SliderPageAdapter.NUM_ITEMS) {
                            currentPageSlider = 0;
                        }
                        vpSlider.setCurrentItem(currentPageSlider++, true);
                    }
                });
            }
        }, 0, 5000);
    }
}
