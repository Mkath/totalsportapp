package com.totalsportapp.ui.userprofile;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.model.SolicitudEquipoAPI;
import com.totalsportapp.data.db.model.Equipo;
import com.totalsportapp.data.db.model.Gender;
import com.totalsportapp.data.db.model.Usuario;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.util.GlideApp;
import com.totalsportapp.util.NetworkUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.totalsportapp.util.AppConstants.BASE_URL_PHOTO;
import static com.totalsportapp.util.AppConstants.DATE_FORMAT_CLIENT;

public class UserProfileActivity extends BaseAppCompatActivity implements View.OnClickListener {

    public static final String KEY_ID_USER = "KEY_ID_USER";
    public static final String KEY_TYPE_VIEW = "KEY_TYPE_VIEW";
    public static final String KEY_TEAM_NAME = "KEY_TEAM_NAME";
    public static final String KEY_REQUEST_ID = "KEY_REQUEST_ID";
    public static final String KEY_REQUEST_COMMENTS = "KEY_REQUEST_COMMENTS";

    public static final int TYPE_PROFILE = 1;
    public static final int TYPE_PROFILE_ADD = 2;
    public static final int TYPE_PROFILE_JOIN_TEAM = 3;

    //region controls
    private Toolbar toolbar;
    private TextView txtv_msg;
    private TextView txtv_fullname;
    private TextView txtv_nickname;
    private TextView txtv_birthday;
    private TextView txtv_gender;
    private TextView txtv_mail;
    private TextView txtv_emergency;
    private LinearLayout ll_container_accept_request;
    private ImageView imgv_team_photo;
    private Button btn_accept_request;
    private Button btn_deny_request;
    private RelativeLayout rl_mail;
    private RelativeLayout rl_emergency;
    private TextView txtv_comments;
    private View separator_comments;
    private RatingBar ratingBar;
    //endregion

    private int typeView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        onCreateBase();

        findControls();

        typeView = getIntent().getExtras().getInt(KEY_TYPE_VIEW);

        configureControls();

        if(NetworkUtils.isNetworkConnected(ctx)){
            loadUserProfile();
        }else{
            showDisconnectedNetwork();
            onBackPressed();
        }
    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        txtv_msg = findViewById(R.id.txtv_msg);
        btn_accept_request = findViewById(R.id.btn_accept_request);
        btn_deny_request = findViewById(R.id.btn_deny_request);
        imgv_team_photo = findViewById(R.id.imgv_team_photo);
        txtv_fullname = findViewById(R.id.txtv_fullname);
        txtv_nickname = findViewById(R.id.txtv_nickname);
        txtv_birthday = findViewById(R.id.txtv_birthday);
        txtv_gender = findViewById(R.id.txtv_gender);
        txtv_mail = findViewById(R.id.txtv_mail);
        txtv_emergency = findViewById(R.id.txtv_emergency);
        ll_container_accept_request = findViewById(R.id.ll_container_accept_request);
        rl_mail = findViewById(R.id.rl_mail);
        rl_emergency = findViewById(R.id.rl_emergency);
        txtv_comments = findViewById(R.id.txtv_comments);
        separator_comments = findViewById(R.id.separator_comments);
        ratingBar = findViewById(R.id.rating_team);
    }

    private void configureControls(){
        setUpToolbar();

        btn_accept_request.setOnClickListener(this);
        btn_deny_request.setOnClickListener(this);
        rl_mail.setOnClickListener(this);
        rl_emergency.setOnClickListener(this);
        imgv_team_photo.setClipToOutline(true);
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Perfil de Usuario");
    }

    private void btn_accept_request_click(){
        if(typeView == TYPE_PROFILE_ADD){

        } else if(typeView == TYPE_PROFILE_JOIN_TEAM){
            confirmAlert("¿Aprobar?", new ExecuteParam() {
                @Override
                public void onExecute() {
                    int requestId = getIntent().getExtras().getInt(KEY_REQUEST_ID);
                    requestTeamResponse(requestId,1);
                }
            });
        }
    }

    private void btn_deny_request_click(){
        if(typeView == TYPE_PROFILE_ADD){

        } else if(typeView == TYPE_PROFILE_JOIN_TEAM){
            confirmAlert("¿Rechazar?", new ExecuteParam() {
                @Override
                public void onExecute() {
                    int requestId = getIntent().getExtras().getInt(KEY_REQUEST_ID);
                    requestTeamResponse(requestId,0);
                }
            });
        }
    }

    private void requestTeamResponse(int idSolicitud, final int aprueba){
        progressDialog.show();

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<OperationResult> call = authServicesAPI.responseRequests(idSolicitud, SolicitudEquipoAPI.TIPO_SOLICITUD_UNIR, aprueba);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()){
                    OperationResult op = response.body();
                    if(op.isStatus()){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                        if(aprueba == 1){
                            Toast.makeText(ctx, "Solicitud Aprobada", Toast.LENGTH_SHORT).show();
                        } else if (aprueba == 0){
                            Toast.makeText(ctx, "Solicitud Rechazada", Toast.LENGTH_SHORT).show();
                        }

                        setResult(RESULT_OK);
                        finish();
                    }else{
                        showResultSaveResponseFail(op.getErrors());
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                showInternalError();
            }
        });
    }

    public void showResultSaveResponseFail(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert(errors.get(0));
    }

    private void rl_emergency_click(){
        if(txtv_emergency.getTag() != null){
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("emergencia", txtv_emergency.getTag().toString());
            clipboard.setPrimaryClip(clip);

            Toast.makeText(ctx, "Copiado", Toast.LENGTH_SHORT).show();
        }
    }

    private void rl_mail_click(){
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("mail", txtv_mail.getText().toString());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(ctx, "Copiado", Toast.LENGTH_SHORT).show();
    }

    private void loadUserProfile(){
        int idUser = getIntent().getExtras().getInt(KEY_ID_USER);

        progressDialog.show();

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<Usuario> call = authServicesAPI.userProfile(idUser);
        call.enqueue(new Callback<Usuario>() {
            @Override
            public void onResponse(Call<Usuario> call, Response<Usuario> response) {

                if(response.isSuccessful()){
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }

                    Usuario usuario = response.body();

                    if(typeView == TYPE_PROFILE){
                        ll_container_accept_request.setVisibility(View.GONE);
                    }else if(typeView == TYPE_PROFILE_ADD){
                        String mgsText = "<b>" + usuario.getNombreCompleto() +"</b> te envió una solicitud de amistad.";
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            txtv_msg.setText(Html.fromHtml(mgsText, Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            txtv_msg.setText(Html.fromHtml(mgsText));
                        }
                        ll_container_accept_request.setVisibility(View.VISIBLE);
                    } else if(typeView == TYPE_PROFILE_JOIN_TEAM){
                        String mgsText = "<b>" + usuario.getNombreCompleto() +"</b> quiere integrarse a tu equipo <b>\"" + getIntent().getExtras().getString(KEY_TEAM_NAME) + "\".</b>";
                        String commentaries = getIntent().getExtras().getString(KEY_REQUEST_COMMENTS);

                        if(commentaries != null && commentaries.isEmpty() == false){
                            commentaries = "<b>Comentarios:</b><br/>" + commentaries + ".";
                        }else{
                            commentaries = "<b>Comentarios:</b><br/> Sin comentarios.";
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            txtv_msg.setText(Html.fromHtml(mgsText, Html.FROM_HTML_MODE_COMPACT));
                            txtv_comments.setText(Html.fromHtml(commentaries, Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            txtv_msg.setText(Html.fromHtml(mgsText));
                            txtv_comments.setText(Html.fromHtml(commentaries));
                        }

                        txtv_comments.setVisibility(View.VISIBLE);
                        separator_comments.setVisibility(View.VISIBLE);
                        ll_container_accept_request.setVisibility(View.VISIBLE);
                    }

                    if(usuario.getFoto() != null){
                        imgv_team_photo.setPadding(0,0,0,0);
                        String photoUrl =  usuario.getFoto();
                        //have to build project if GlideApp is not found
                        GlideApp.with(ctx)
                                .load(photoUrl)
                                .dontAnimate()
                                .centerCrop()
                                .into(imgv_team_photo);
                    }

                    txtv_fullname.setText(usuario.getNombreCompleto());
                    if(usuario.getAlias() != null){
                        txtv_nickname.setText(usuario.getAlias());
                    }

                    if(usuario.getFechaNacimiento() != null){
                        txtv_birthday.setText(usuario.getFechaNacimiento().format(DATE_FORMAT_CLIENT));
                    }else{
                        txtv_birthday.setText("-");
                    }
                    if(!usuario.getCalificacion().equals("")){
                        ratingBar.setRating(Float.valueOf(usuario.getCalificacion()));
                    }else {
                        ratingBar.setRating(0);
                    }

                    if(usuario.getSexo() != null){
                        if(usuario.getSexo() == Gender.MAN){
                            txtv_gender.setText("Masculino");
                        }else if(usuario.getSexo() == Gender.WOMAN) {
                            txtv_gender.setText("Femenino");
                        }
                    }else{
                        txtv_gender.setText("-");
                    }

                    txtv_mail.setText(usuario.getEmail());

                    if(usuario.getContactoEmergencia() != null){
                        txtv_emergency.setText(usuario.getContactoEmergencia());
                        txtv_emergency.setTag(usuario.getContactoEmergencia());
                    }else{
                        txtv_emergency.setText("-");
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<Usuario> call, Throwable t) {
                showInternalError();
            }
        });
    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_accept_request:
                btn_accept_request_click();
                break;
            case R.id.btn_deny_request:
                btn_deny_request_click();
                break;
            case R.id.rl_emergency:
                rl_emergency_click();
                break;
            case R.id.rl_mail:
                rl_mail_click();
                break;
        }
    }
}
