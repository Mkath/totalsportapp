package com.totalsportapp.ui.encuestas;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.totalsportapp.R;
import com.totalsportapp.data.api.model.MiembroParticipante;
import com.totalsportapp.data.api.model.ParticipantModel;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.util.LoaderAdapter;
import com.totalsportapp.util.OnClickListListener;

import java.util.ArrayList;


/**
 * Created by katherine on 15/05/17.
 */

public class EncuestaAdapter extends LoaderAdapter<MiembroParticipante> implements OnClickListListener {

    private Context context;
    private ArrayList<ParticipantModel> list;
    private String cadena;
    private int idPregunta;

    public EncuestaAdapter(ArrayList<MiembroParticipante> preguntaEntities, Context context) {
        super(context);
        setItems(preguntaEntities);
        this.context = context;
        list = new ArrayList<>();
    }

    public ArrayList<MiembroParticipante> getItems() {
        return (ArrayList<MiembroParticipante>) getmItems();
    }

    @Override
    public long getYourItemId(int position) {
        return getmItems().get(position).getIdMiembroInvitacion();
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_encuesta, parent, false);
        return new ViewHolder(root, this);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void bindYourViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final MiembroParticipante miembroParticipante = getItems().get(position);

        ((ViewHolder) holder).tvName.setText(miembroParticipante.getUsuarioNombre()+" " + miembroParticipante.getUsuarioApellido());
        ((ViewHolder) holder).btn_aistio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewHolder) holder).btn_aistio.setBackground(context.getResources().getDrawable(R.drawable.button_selected));
                ((ViewHolder) holder).btn_no_asistio.setBackground(context.getResources().getDrawable(R.drawable.button_border));
                ((ViewHolder) holder).btn_aistio.setTextColor(context.getResources().getColor(R.color.white));
                ((ViewHolder) holder).btn_no_asistio.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                if(PreferencesHelper.getInstance().getArrayListEncuesta(PreferencesHelper.ARRAY_ENCUESTA)==null){
                    list.add(new ParticipantModel(miembroParticipante.getIdUsuario(),1));
                    PreferencesHelper.getInstance().saveArrayListEncuesta(list);

                }else{
                    ArrayList<ParticipantModel> newList = new ArrayList<>();
                    newList = PreferencesHelper.getInstance().getArrayListEncuesta(PreferencesHelper.ARRAY_ENCUESTA);
                    for (int i = 0; i <newList.size() ; i++) {
                        if(newList.get(i).getIdUsuario() == miembroParticipante.getIdUsuario()){
                            newList.remove(i);
                        }
                    }
                    list = newList;
                    list.add(new ParticipantModel(miembroParticipante.getIdUsuario(),1));
                    PreferencesHelper.getInstance().saveArrayListEncuesta(list);
                }
            }
        });

        ((ViewHolder) holder).btn_no_asistio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewHolder) holder).btn_aistio.setBackground(context.getResources().getDrawable(R.drawable.button_border));
                ((ViewHolder) holder).btn_no_asistio.setBackground(context.getResources().getDrawable(R.drawable.button_selected));
                ((ViewHolder) holder).btn_aistio.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                ((ViewHolder) holder).btn_no_asistio.setTextColor(context.getResources().getColor(R.color.white));

                if(PreferencesHelper.getInstance().getArrayListEncuesta(PreferencesHelper.ARRAY_ENCUESTA)==null){
                    list.add(new ParticipantModel(miembroParticipante.getIdUsuario(),0));
                    PreferencesHelper.getInstance().saveArrayListEncuesta(list);

                }else{
                    ArrayList<ParticipantModel> newList = new ArrayList<>();
                    newList = PreferencesHelper.getInstance().getArrayListEncuesta(PreferencesHelper.ARRAY_ENCUESTA);
                    for (int i = 0; i <newList.size() ; i++) {
                        if(newList.get(i).getIdUsuario() == miembroParticipante.getIdUsuario()){
                            newList.remove(i);
                        }
                    }
                    list = newList;
                    list.add(new ParticipantModel(miembroParticipante.getIdUsuario(), 0));
                    PreferencesHelper.getInstance().saveArrayListEncuesta(list);
                }
            }
        });

        /* if (miembroParticipante.getIdTipoPregunta() == 1) {
            ((ViewHolder) holder).tvQuestion.setText(preguntaEntity.getDescripcion());
            ((ViewHolder) holder).buttonYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ViewHolder) holder).buttonYes.setBackground(context.getResources().getDrawable(R.drawable.button_selected));
                    ((ViewHolder) holder).buttonYes.setTextColor(context.getResources().getColor(R.color.white));
                    ((ViewHolder) holder).buttonNo.setBackground(context.getResources().getDrawable(R.drawable.button_border));
                    ((ViewHolder) holder).buttonNo.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    if(mSessionManager.getArrayList(SessionManager.ARRAY_ENCUESTA)==null){
                        list.add(new RespuestaEntity(preguntaEntity.getIdPregunta(), "Si"));
                        mSessionManager.saveArrayList(list);

                    }else{
                        ArrayList<RespuestaEntity> newList = new ArrayList<>();
                        newList = mSessionManager.getArrayList(SessionManager.ARRAY_ENCUESTA);
                        for (int i = 0; i <newList.size() ; i++) {
                            if(newList.get(i).getIdPregunta() == preguntaEntity.getIdPregunta()){
                                newList.remove(i);
                            }
                        }
                        list = newList;
                        list.add(new RespuestaEntity(preguntaEntity.getIdPregunta(), "Si"));
                        mSessionManager.saveArrayList(list);
                    }
                }
            });
            ((ViewHolder) holder).buttonNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ViewHolder) holder).buttonNo.setBackground(context.getResources().getDrawable(R.drawable.button_selected));
                    ((ViewHolder) holder).buttonNo.setTextColor(context.getResources().getColor(R.color.white));
                    ((ViewHolder) holder).buttonYes.setBackground(context.getResources().getDrawable(R.drawable.button_border));
                    ((ViewHolder) holder).buttonYes.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    if(mSessionManager.getArrayList(SessionManager.ARRAY_ENCUESTA)==null){
                        list.add(new RespuestaEntity(preguntaEntity.getIdPregunta(), "No"));
                        mSessionManager.saveArrayList(list);

                    }else{
                        ArrayList<RespuestaEntity> newList = new ArrayList<>();
                        newList = mSessionManager.getArrayList(SessionManager.ARRAY_ENCUESTA);
                        for (int i = 0; i <newList.size() ; i++) {
                            if(newList.get(i).getIdPregunta() == preguntaEntity.getIdPregunta()){
                                newList.remove(i);
                            }
                        }
                        list = newList;
                        list.add(new RespuestaEntity(preguntaEntity.getIdPregunta(), "No"));
                        mSessionManager.saveArrayList(list);
                    }
                }
            });

        } else {
            if (preguntaEntity.getIdTipoPregunta() == 2) {
                ((ViewHolder) holder).lyQuestion.setVisibility(View.GONE);
                ((ViewHolder) holder).buttons.setVisibility(View.GONE);
                ((ViewHolder) holder).lyComentarios.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).etComentarios.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).etComentarios.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        mSessionManager.setIdComentario(preguntaEntity.getIdPregunta());
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        cadena = ((ViewHolder) holder).etComentarios.getText().toString();
                        mSessionManager.setComentario(cadena);
                    }
                });

            } else {
                ((ViewHolder) holder).lyQuestion.setVisibility(View.GONE);
                ((ViewHolder) holder).buttons.setVisibility(View.GONE);
                ((ViewHolder) holder).rlCalificanos.setVisibility(View.VISIBLE);
                ((ViewHolder) holder).rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                        //list.add(new RespuestaEntity(idPregunta, cadena));
                        //list.add(new RespuestaEntity(preguntaEntity.getIdPregunta(), String.valueOf(((ViewHolder) holder).rating.getRating())) );
                        mSessionManager.setIdCalificanos(preguntaEntity.getIdPregunta());
                        mSessionManager.setRating(String.valueOf(((ViewHolder) holder).rating.getRating()));
                        //mSessionManager.saveArrayList(list);
                    }
                });

            }
        }*/

    }

    @Override
    public void onClick(int position) {

        // MenuEntity menuEntity = getItems().get(position);
        //  menuItem.clickItem(menuEntity);
    }


    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private OnClickListListener onClickListListener;
        private TextView tvName;
        private Button btn_aistio;
        private Button btn_no_asistio;

        ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            this.onClickListListener = onClickListListener;
            this.itemView.setOnClickListener(this);
            tvName = itemView.findViewById(R.id.tv_name);
            btn_aistio = itemView.findViewById(R.id.button_asistio);
            btn_no_asistio = itemView.findViewById(R.id.button_no_asistio);
        }


        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
