package com.totalsportapp.ui.encuestas;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.totalsportapp.R;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.team.TeamsTabFragment;
import com.totalsportapp.util.ActivityUtils;

public class EncuestaActivity extends BaseAppCompatActivity {

    private EncuestaFragment fragment;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_back);

        toolbar = findViewById(R.id.toolbar);

        toolbar.setTitle("Califica tu reto");

        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setDisplayShowHomeEnabled(true);

        fragment = (EncuestaFragment) getSupportFragmentManager()
                .findFragmentById(R.id.body);

        if (fragment == null) {
            fragment = EncuestaFragment.newInstance(getIntent().getExtras());

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    fragment, R.id.body);
        }

        // Create the presenter
        //new NewQuestionPresenter(fragment, this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
