package com.totalsportapp.ui.challengeteam;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.totalsportapp.MainApplication;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.FilterTeamAppRequest;
import com.totalsportapp.data.api.model.RequestModel;
import com.totalsportapp.data.db.model.Equipo;
import com.totalsportapp.data.db.model.EquipoDao;
import com.totalsportapp.data.db.model.EquipoLugar;
import com.totalsportapp.data.db.model.NivelEquipo;
import com.totalsportapp.data.db.model.Ubigeo;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.jointeam.JoinTeamActivity;
import com.totalsportapp.ui.jointeam.JoinTeamAdapter;
import com.totalsportapp.ui.jointeam.JoinTeamInterface;
import com.totalsportapp.ui.main.tabs.TeamAdapter;
import com.totalsportapp.ui.teamprofile.TeamProfileActivity;
import com.totalsportapp.util.ListUtils;
import com.totalsportapp.util.NetworkUtils;
import com.totalsportapp.util.RecyclerViewScrollListener;
import com.totalsportapp.util.ScrollChildSwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChallengeTeamActivity extends BaseAppCompatActivity implements View.OnClickListener, JoinTeamInterface {

    //region controls
    private DrawerLayout drawerLayoutFilter;
    private Toolbar toolbar;

    private ExpandableLinearLayout ell_levels;
    private ExpandableLinearLayout ell_locations;

    private RelativeLayout rl_container_level_filter;
    private RelativeLayout rl_container_location_filter;

    private RelativeLayout rl_empty_teams;
    private RecyclerView lv_teams;

    private Spinner spinner_departamento;
    private Spinner spinner_provincia;

    private Button check_all_levels;
    private Button clean_all_levels;

    private Button check_all_locations;
    private Button clean_all_locations;
    private LinearLayout container_buttons;
    private LinearLayout container_zonas;

    private LinearLayoutManager mlinearLayoutManager;
    private int currentPage = 1;
    private int currentPageFilter = 1;
    private boolean mFirstLoad = false;
    private JoinTeamAdapter mJoinTeamAdapter;
    private int totalPage;
    private int count = 0;
    private FilterTeamAppRequest filterTeamAppRequestApply;
    private boolean isFirstDepartamento = true;
    private boolean isFirstProvincia = true;
    ArrayList<Ubigeo> departamentos;
    ArrayList<Ubigeo> provincias;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_team);

        onCreateBase();

        findControls();

        configureControls();


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(NetworkUtils.isNetworkConnected(ctx)){
            startLoad();
        }else{
            Toast.makeText(ctx, R.string.disconnected_network, Toast.LENGTH_LONG).show();
            onBackPressed();
        }
    }

    private FilterTeamAppRequest getFilterTeamAppRequest(int page){
        FilterTeamAppRequest filterTeamAppRequest = new FilterTeamAppRequest();
         ArrayList<String> ubigeos = new ArrayList<String>();
            ArrayList<Integer> niveles = new ArrayList<>();

            for(int i = 0; i < ell_locations.getChildCount(); i++){
                CheckBox chk =  (CheckBox) ell_locations.getChildAt(i);
                if(chk.isChecked()){
                    ubigeos.add(chk.getTag().toString());
                }
            }

            for(int i = 0; i < ell_levels.getChildCount(); i++){
                CheckBox chk =  (CheckBox) ell_levels.getChildAt(i);
                if(chk.isChecked()){
                    niveles.add((int)chk.getTag());
                }
            }

            filterTeamAppRequest.setTipoFiltro(FilterTeamAppRequest.FILTER_TYPE_CHALLENGE);
            filterTeamAppRequest.setUbigeos(ubigeos);
            filterTeamAppRequest.setNiveles(niveles);
            filterTeamAppRequest.setPagina(page);

        return filterTeamAppRequest;
    }

    public void startLoad() {
        if (!mFirstLoad) {
            displayTeams(1);
            mFirstLoad = true;
        }
    }

    public void refreshTeams() {
        currentPage = 1;
        displayTeams(1);
    }

    private void displayTeams(){
        displayTeams(currentPage);
    }

    public void showLoadMore(boolean active) {
        mJoinTeamAdapter.showLoading(active);
    }


    private void displayTeams(int idPage){

        filterTeamAppRequestApply = getFilterTeamAppRequest(idPage);


        if (currentPage != -1) {
            AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
            Call<RequestModel> call = null;

            call = services.filterTeams(filterTeamAppRequestApply);
            //}

            if (currentPage == 1) {
                setLoadingIndicator(true);
            } else {
                showLoadMore(true);
            }

            call.enqueue(new Callback<RequestModel>() {
                @Override
                public void onResponse(Call<RequestModel> call, Response<RequestModel> response) {

                    if (currentPage == 1) {
                       // Toast.makeText(ChallengeTeamActivity.this, "page 1" + currentPage, Toast.LENGTH_SHORT).show();
                        setLoadingIndicator(false);
                    } else {
                      //  Toast.makeText(ChallengeTeamActivity.this, "other" + currentPage, Toast.LENGTH_SHORT).show();
                        showLoadMore(false);
                    }


                    if (response.isSuccessful()) {
                        if (currentPage == 1) {
                            count++;
                            showTeams(response.body().getEquipos());
                        } else {
                            if(currentPage !=0){
                                showMoreTeams(response.body().getEquipos());
                            }
                        }

                        totalPage = response.body().getTotalPagina();
                        Log.e("Total páginas", String.valueOf(response.body().getTotalPagina()));
                        Log.e("current page", String.valueOf(currentPage));

                        if (response.body().getTotalPagina() != currentPage) {
                            currentPage++;
                        } else {
                            currentPage = -1;
                        }

                        Log.e("new current page", String.valueOf(currentPage));


                    } else {
                     //   Toast.makeText(ChallengeTeamActivity.this, "not succes" + currentPage, Toast.LENGTH_SHORT).show();
                        showLoadMore(false);
                        showInternalError();
                    }
                }

                @Override
                public void onFailure(Call<RequestModel> call, Throwable t) {


                    if (currentPage == 1) {
                        setLoadingIndicator(false);
                    } else {
                     //   Toast.makeText(ChallengeTeamActivity.this, "failure" + currentPage, Toast.LENGTH_SHORT).show();
                        showLoadMore(false);
                    }

                    showInternalError();
                }
            });
        }
    }

    private void setLoadingIndicator(final boolean active){

        if(active){
            progressDialog.show();
        }else{

            if(progressDialog.isShowing()){
                progressDialog.dismiss();
            }
        }

        final SwipeRefreshLayout srl =
                (SwipeRefreshLayout) findViewById(R.id.scroll);

        // Make sure setRefreshing() is called after the layout is done with everything else.
        srl.post(new Runnable() {
            @Override
            public void run() {
                srl.setRefreshing(active);
            }
        });
    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    private void findControls(){
        drawerLayoutFilter = findViewById(R.id.drawer_layout_filter);
        toolbar = findViewById(R.id.toolbar);
        rl_container_level_filter = findViewById(R.id.rl_container_level_filter);
        rl_container_location_filter = findViewById(R.id.rl_container_location_filter);
        ell_levels = findViewById(R.id.ell_levels);
        ell_locations = findViewById(R.id.ell_locations);
        spinner_departamento = findViewById(R.id.spinner_departamento);
        spinner_provincia = findViewById(R.id.spinner_provincia);
        check_all_levels = findViewById(R.id.check_all_levels);
        clean_all_levels = findViewById(R.id.clean_all_levels);

        check_all_locations = findViewById(R.id.check_all_locations);
        clean_all_locations = findViewById(R.id.clean_all_locations);
        container_buttons = findViewById(R.id.container_buttons);
        container_zonas = findViewById(R.id.container_zonas);
        rl_empty_teams = findViewById(R.id.rl_empty_teams);
        lv_teams = findViewById(R.id.lv_teams);

        final ScrollChildSwipeRefreshLayout swipeRefreshLayout =
                (ScrollChildSwipeRefreshLayout) findViewById(R.id.scroll);
        swipeRefreshLayout.setColorSchemeColors(
                ContextCompat.getColor(this, R.color.colorPrimary),
                ContextCompat.getColor(this, R.color.colorPrimaryDark),
                ContextCompat.getColor(this, R.color.colorAccent)
        );
        // Set the scrolling view in the custom SwipeRefreshLayout.
        swipeRefreshLayout.setScrollUpChild(lv_teams);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                count = 0;
                refreshTeams();
            }
        });

        mJoinTeamAdapter = new JoinTeamAdapter(getApplicationContext(), new ArrayList<Equipo>(), this, ChallengeTeamActivity.this);
        mJoinTeamAdapter.setListType(TeamAdapter.LIST_TYPE_CHALLENGE_TEAM);
        mlinearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mlinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        lv_teams.setAdapter(mJoinTeamAdapter);
        lv_teams.setLayoutManager(mlinearLayoutManager);
    }

    private void configureControls() {
        setUpToolbar();

        rl_container_level_filter.setOnClickListener(this);
        rl_container_location_filter.setOnClickListener(this);
        clean_all_levels.setOnClickListener(this);
        check_all_levels.setOnClickListener(this);

        clean_all_locations.setOnClickListener(this);
        check_all_locations.setOnClickListener(this);

        setUpLocationFilter();

        setUpLevelFilter();

        setDepartamentoSpinner();

        setProvinciaSpinner();

        setUpDrawerLayout();
    }

    private void setUpDrawerLayout(){
        drawerLayoutFilter.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View view, float v) {
            }

            @Override
            public void onDrawerOpened(View view) {
            }

            @Override
            public void onDrawerClosed(View view) {
                FilterTeamAppRequest  newFilterApply = getFilterTeamAppRequest(1);
                boolean eqUbigeo = ListUtils.equalListString(newFilterApply.getUbigeos(), filterTeamAppRequestApply.getUbigeos());
                boolean eqNivel = ListUtils.equalListInt(newFilterApply.getNiveles(), filterTeamAppRequestApply.getNiveles());

                if (eqUbigeo != true || eqNivel != true){
                    count = 0;
                    refreshTeams();
                }
            }

            @Override
            public void onDrawerStateChanged(int i) {
            }
        });
    }

    private void displayTeamsFilter(FilterTeamAppRequest filterTeamAppRequest){


        if (currentPageFilter!= -1) {
            AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
            Call<RequestModel> call = null;

            call = services.filterTeams(filterTeamAppRequest);
            //}

            if (currentPageFilter == 1) {
                setLoadingIndicator(true);
            } else {
                showLoadMore(true);
            }

            call.enqueue(new Callback<RequestModel>() {
                @Override
                public void onResponse(Call<RequestModel> call, Response<RequestModel> response) {

                    if (currentPageFilter == 1) {
                        Toast.makeText(ChallengeTeamActivity.this, "page 1" + currentPageFilter, Toast.LENGTH_SHORT).show();
                        setLoadingIndicator(false);
                    } else {
                        Toast.makeText(ChallengeTeamActivity.this, "other" + currentPageFilter, Toast.LENGTH_SHORT).show();
                        showLoadMore(false);
                    }


                    if (response.isSuccessful()) {
                        if (currentPageFilter == 1) {
                            showTeams(response.body().getEquipos());
                        } else {
                            showMoreTeams(response.body().getEquipos());
                        }

                        if (response.body().getTotalPagina() != currentPage) {
                            currentPageFilter++;
                        } else {
                            currentPageFilter = -1;
                        }


                    } else {
                        Toast.makeText(ChallengeTeamActivity.this, "not succes" + currentPageFilter, Toast.LENGTH_SHORT).show();
                        showLoadMore(false);
                        showInternalError();
                    }
                }

                @Override
                public void onFailure(Call<RequestModel> call, Throwable t) {


                    if (currentPageFilter == 1) {
                        setLoadingIndicator(false);
                    } else {
                        Toast.makeText(ChallengeTeamActivity.this, "failure" + currentPageFilter, Toast.LENGTH_SHORT).show();
                        showLoadMore(false);
                    }

                    showInternalError();
                }
            });
        }
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Retando Equipos");
    }

    private void setUpLocationFilter(){

        List<Equipo> equipos = getDaoSession().getEquipoDao().queryBuilder().where(EquipoDao.Properties.EsLider.eq(true)).list();

        List<Ubigeo> ubigeos = ((MainApplication)getApplication()).getDaoSession().getUbigeoDao().loadAll();

        for(Ubigeo ubigeo : ubigeos){
            CheckBox checkBox = (CheckBox) getLayoutInflater().inflate(R.layout.layout_item_filter, null);
            checkBox.setText(ubigeo.getNombre());
            checkBox.setTag(ubigeo.getCodigo());

            outerloop:
            for(Equipo equipo : equipos){
                for(EquipoLugar equipoLugar : equipo.getLugares()){
                    if(equipoLugar.getUbigeo().equalsIgnoreCase(ubigeo.getCodigo())){
                        checkBox.setChecked(true);
                        break outerloop;
                    }
                }
            }

            ell_locations.addView(checkBox);
        }
    }

    private void setUpLevelFilter(){
        List<Equipo> equipos = getDaoSession().getEquipoDao().queryBuilder().where(EquipoDao.Properties.EsLider.eq(true)).list();

        List<NivelEquipo> nivelesEquipo = NivelEquipo.getNivelesEquipo();

        for(NivelEquipo nivelEquipo : nivelesEquipo){
            CheckBox checkBox = (CheckBox) getLayoutInflater().inflate(R.layout.layout_item_filter, null);
            checkBox.setText(nivelEquipo.getDescripcion());
            checkBox.setTag(nivelEquipo.getId());

            for(Equipo equipo : equipos){
                if(equipo.getNivel() == nivelEquipo.getId()){
                    checkBox.setChecked(true);
                }
            }

            ell_levels.addView(checkBox);
        }
    }

    private void showTeams(ArrayList<Equipo> list){
        mJoinTeamAdapter.setItems(list);
        rl_empty_teams.setVisibility(View.GONE);

        if (this.lv_teams != null && mJoinTeamAdapter != null) {
            mJoinTeamAdapter.setItems(list);

            if (list.size() > 0) {
                rl_empty_teams.setVisibility(View.GONE);
            } else {
                rl_empty_teams.setVisibility(View.VISIBLE);
            }

            this.lv_teams.addOnScrollListener(new RecyclerViewScrollListener() {
                @Override
                public void onScrollUp() {

                }

                @Override
                public void onScrollDown() {

                }

                @Override
                public void onLoadMore() {
                    if (count < totalPage) {
                        count++;
                        Log.e("Contador", String.valueOf(count));
                        displayTeams();
                    }
                }
            });


        } else {
            rl_empty_teams.setVisibility(View.VISIBLE);
        }
    }

    private void showMoreTeams(ArrayList<Equipo> list){
        ArrayList<Equipo> postAux = (ArrayList<Equipo>) mJoinTeamAdapter.getmItems();
        postAux.addAll(list);
        mJoinTeamAdapter.setItems(postAux);
        mJoinTeamAdapter.notifyDataSetChanged();
    }


    private void setDepartamentoSpinner() {

        final List<String> list = new ArrayList<String>();

        departamentos = PreferencesHelper.getInstance().getArrayListDepartamentos(PreferencesHelper.ARRAY_DEPARTAMENTOS);

        for (int i = 0; i < departamentos.size(); i++) {
            list.add(departamentos.get(i).getNombre());
        }


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_departamento.setAdapter(dataAdapter);

        spinner_departamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (isFirstDepartamento) {
                    isFirstDepartamento = false;
                } else {
                    getProvinciaById(spinner_departamento.getItemAtPosition(i).toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_departamento.setSelection(14);


    }

    public void getProvinciaById(String departamento) {

        String id = "150000";

        for (int i = 0; i < departamentos.size(); i++) {
            if (departamentos.get(i).getNombre().equals(departamento)) {
                id = departamentos.get(i).getCodigo();
            }
        }

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<Ubigeo>> call = authServicesAPI.getProvincias(id);
        call.enqueue(new Callback<ArrayList<Ubigeo>>() {
            @Override
            public void onResponse(Call<ArrayList<Ubigeo>> call, Response<ArrayList<Ubigeo>> response) {
                if (response.isSuccessful()) {
                    provincias = response.body();
                    setNewProvincias(response.body());
                } else {
                    Toast.makeText(ChallengeTeamActivity.this, "No se encontraron provincias, intente nuevamente", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Ubigeo>> call, Throwable t) {
                Toast.makeText(ChallengeTeamActivity.this, "Ha ocurrido un error al obtener provincias", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void setProvinciaSpinner() {

        final List<String> list = new ArrayList<String>();

        provincias = PreferencesHelper.getInstance().getArrayListProvincias(PreferencesHelper.ARRAY_PROVINCIAS);

        for (int i = 0; i < provincias.size(); i++) {
            list.add(provincias.get(i).getNombre());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_provincia.setAdapter(dataAdapter);

        spinner_provincia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (isFirstProvincia) {
                    isFirstProvincia = false;
                } else {
                    getDistritoById(spinner_provincia.getItemAtPosition(i).toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_provincia.setSelection(7);


    }

    private void setNewProvincias(ArrayList<Ubigeo> listaProvincias) {
        final List<String> list = new ArrayList<String>();

        for (int i = 0; i < listaProvincias.size(); i++) {
            list.add(listaProvincias.get(i).getNombre());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_provincia.setAdapter(dataAdapter);

        spinner_provincia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (isFirstProvincia) {
                    isFirstProvincia = false;
                } else {
                    getDistritoById(spinner_provincia.getItemAtPosition(i).toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    public void getDistritoById(String provincia) {

        String id = "150100";

        for (int i = 0; i < provincias.size(); i++) {
            if (provincias.get(i).getNombre().equals(provincia)) {
                id = provincias.get(i).getCodigo();
            }
        }

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<Ubigeo>> call = authServicesAPI.getDistritos(id);
        call.enqueue(new Callback<ArrayList<Ubigeo>>() {
            @Override
            public void onResponse(Call<ArrayList<Ubigeo>> call, Response<ArrayList<Ubigeo>> response) {
                if (response.isSuccessful()) {

                    ell_locations.removeAllViews();

                    for (Ubigeo ubigeo : response.body()) {
                        CheckBox checkBox = (CheckBox) getLayoutInflater().inflate(R.layout.layout_item_filter, null);
                        checkBox.setText(ubigeo.getNombre());
                        checkBox.setTag(ubigeo.getCodigo());
                        ell_locations.addView(checkBox);
                    }
                } else {
                    Toast.makeText(ChallengeTeamActivity.this, "No se encontraron distritos, intente nuevamente", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Ubigeo>> call, Throwable t) {
                Toast.makeText(ChallengeTeamActivity.this, "Ha ocurrido un error al obtener los distritos", Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_filter_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;

            case R.id.filter_action:
                if (drawerLayoutFilter.isDrawerOpen(Gravity.RIGHT)) {
                    drawerLayoutFilter.closeDrawer(Gravity.RIGHT);
                } else {
                    drawerLayoutFilter.openDrawer(Gravity.RIGHT);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_container_level_filter:

                ell_levels.toggle();

                if (container_buttons.getVisibility() == View.VISIBLE) {
                    container_buttons.setVisibility(View.GONE);
                } else {
                    container_buttons.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.rl_container_location_filter:

                if (container_zonas.getVisibility() == View.VISIBLE) {
                   /* Animation slideBottom = AnimationUtils.loadAnimation(this, R.anim.slide_bottom);
                    container_zonas.startAnimation(slideBottom);
                    */
                    container_zonas.setVisibility(View.GONE);

                } else {

                  /*  Animation slideUp = AnimationUtils.loadAnimation(this, R.anim.slide_up);
                   container_zonas.startAnimation(slideUp);*/
                    container_zonas.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.clean_all_levels:
                cleanCheckboxLeevel();
                break;
            case R.id.check_all_levels:
                checkedAllCheckboxLeevel();
                break;

            case R.id.clean_all_locations:
                cleanCheckboxLocation();
                break;
            case R.id.check_all_locations:
                checkedAllCheckboxLocation();
                break;
        }
    }

    private void cleanCheckboxLocation() {

        for (int i = 0; i < ell_locations.getChildCount(); i++) {
            CheckBox chk = (CheckBox) ell_locations.getChildAt(i);
            chk.setChecked(false);
        }
    }


    private void cleanCheckboxLeevel() {

        for (int i = 0; i < ell_levels.getChildCount(); i++) {
            CheckBox chk = (CheckBox) ell_levels.getChildAt(i);
            chk.setChecked(false);

        }
    }

    private void checkedAllCheckboxLocation() {

        for (int i = 0; i < ell_locations.getChildCount(); i++) {
            CheckBox chk = (CheckBox) ell_locations.getChildAt(i);
            chk.setChecked(true);
        }
    }


    private void checkedAllCheckboxLeevel() {

        for (int i = 0; i < ell_levels.getChildCount(); i++) {
            CheckBox chk = (CheckBox) ell_levels.getChildAt(i);
            chk.setChecked(true);
        }
    }

    @Override
    public void onClick(Equipo equipo) {
        Intent i = new Intent(ctx, TeamProfileActivity.class);
        i.putExtra(TeamProfileActivity.KEY_TEAM_ID, equipo.getIdEquipo());
        i.putExtra(TeamProfileActivity.KEY_TYPE_TEAM_PROFILE, TeamProfileActivity.TYPE_CHALLENGE_TEAM);
        ctx.startActivity(i);
    }
}
