package com.totalsportapp.ui.login;

import com.totalsportapp.data.api.model.TokenAPI;

import java.util.List;

import hirondelle.date4j.DateTime;

public interface LoginView {
    //void saveAuthToken(String accessToken, String refreshToken, DateTime expireAt);
    void setLoggedMode(int mode);

    void onResultLoginSuccess();
    void showResultLoginFail(String messageError);

    void showLoginFormValidateError(String messague);
    void showProcessingLogin();
    void showInternalError();
}
