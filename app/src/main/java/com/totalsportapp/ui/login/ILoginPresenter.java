package com.totalsportapp.ui.login;

import com.totalsportapp.data.api.model.LoginRequest;
import com.totalsportapp.data.api.model.RegisterRequest;

public interface ILoginPresenter {

    void LogIn(LoginRequest loginRequest);

    void Register(RegisterRequest registerRequest);
}
