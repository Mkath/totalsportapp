package com.totalsportapp.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.totalsportapp.R;
import com.totalsportapp.data.api.model.LoginRequest;
import com.totalsportapp.data.api.model.RegisterRequest;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.main.MainActivity;
import com.totalsportapp.ui.recovery.SendEmailRecoveryActivity;
import com.totalsportapp.ui.register.RegisterActivity;
import com.totalsportapp.ui.start.StartActivity;
import com.totalsportapp.util.GoogleSignInUtil;
import com.totalsportapp.util.NetworkUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import static com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes.SIGN_IN_CANCELLED;
import static com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes.SIGN_IN_FAILED;

public class LoginActivity extends BaseAppCompatActivity implements View.OnClickListener, LoginView {

    private final static int RC_SIGN_IN_GOOGLE = 100;
    private ILoginPresenter presenter;

    //region controls

    private Button btnLogin;
    private Button btnLoginFb;
    private Button btnLoginGoogle;
    private TextView txtvForgotPwd;
    private TextView txtvWithoutAccount;
    private EditText etxtPassword;
    private EditText etxtUsername;
    private AutoCompleteTextView actxt_username;

    //endregion

    private GoogleSignInClient mGoogleSignInClient;

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        onCreateBase();

        presenter = new LoginPresenter(this);

        findControls();

        configureControls();

        initializeGoogle();
        initializeFacebook();

        //setFakeData();
    }

    private void setFakeData(){
        etxtUsername.setText("bryan.chauca.6@hotmail.com");
        etxtPassword.setText("123456");
        //etxtUsername.setVisibility(View.GONE);

        String[] emails = new String[] {
                "bryan.chauca.6@hotmail.com", "brayan_che_6@hotmail.com", "bryan.chauca.6@gmail.com"
        };

        actxt_username = findViewById(R.id.actxt_username);
        actxt_username.setVisibility(View.VISIBLE);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, emails);

        actxt_username.setAdapter(adapter);

        actxt_username.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String selected = (String) adapterView.getItemAtPosition(position);
                etxtUsername.setText(selected);
            }
        });
    }

    private void findControls(){
        btnLogin = findViewById(R.id.btn_login);
        btnLoginFb = findViewById(R.id.btn_login_fb);
        btnLoginGoogle = findViewById(R.id.btn_login_google);
        txtvForgotPwd = findViewById(R.id.txtv_forgot_pwd);
        txtvWithoutAccount = findViewById(R.id.txtv_without_account);
        etxtPassword = findViewById(R.id.etxt_password);
        etxtUsername = findViewById(R.id.etxt_username);
    }

    private void configureControls(){
        txtvForgotPwd.setMovementMethod(LinkMovementMethod.getInstance());
        txtvForgotPwd.setOnClickListener(this);
        txtvWithoutAccount.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        btnLoginFb.setOnClickListener(this);
        btnLoginGoogle.setOnClickListener(this);
    }

    private void initializeGoogle(){
        mGoogleSignInClient = GoogleSignInUtil.getGoogleSignInClient(ctx);
    }

    private void initializeFacebook(){
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        callbackFacebookSuccess(loginResult);
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if(exception != null){
                            if(exception.getMessage() != null){
                                simpleAlert( "Error Facebook Ex.", exception.getMessage());
                            }
                        }else{
                            simpleAlert("Error de repuesta de Facebook");
                        }
                    }
                });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

       // Intent i = new Intent(ctx, StartActivity.class);
        //startActivity(i);
    }

    @Override
    public void setLoggedMode(int mode) {
        PreferencesHelper.getInstance().setCurrentUserLoggedInMode(mode);
    }

    @Override
    public void onResultLoginSuccess() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        Toast.makeText(ctx, "¡Bienvenido!", Toast.LENGTH_LONG).show();

        Intent i = new Intent(ctx, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);

        overridePendingTransition(R.anim.anim_enter, R.anim.anim_leave);
    }

    @Override
    public void showResultLoginFail(String messageError) {
        if(progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        simpleAlert(messageError);
    }

    @Override
    public void showLoginFormValidateError(String messague) {
        simpleAlert(messague);
    }

    @Override
    public void showProcessingLogin() {
        progressDialog.show();
    }

    @Override
    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    private void withoutAccount(){
        Intent i = new Intent(ctx, RegisterActivity.class);
        startActivity(i);
    }

    private void forgotPassword(){
        Intent i = new Intent(ctx, SendEmailRecoveryActivity.class);
        startActivity(i);
    }

    private void loginClick(){
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername(etxtUsername.getText().toString());
        loginRequest.setPassword(etxtPassword.getText().toString());

        if(NetworkUtils.isNetworkConnected(ctx)){
            presenter.LogIn(loginRequest);
        }else{
            showDisconnectedNetwork();
        }
    }

    private void loginFacebookClick(){
        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("email", "public_profile", "user_friends"));
    }

    private void loginGoogleClick(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN_GOOGLE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txtv_without_account:
                withoutAccount();
                break;
            case R.id.txtv_forgot_pwd:
                forgotPassword();
                break;
            case R.id.btn_login:
                loginClick();
                break;
            case R.id.btn_login_fb:
                loginFacebookClick();
                break;
            case R.id.btn_login_google:
                loginGoogleClick();
                break;

        }
    }

    private boolean onActivityResultGoogleSignIn(int requestCode, Intent data){
        if(requestCode == RC_SIGN_IN_GOOGLE){
            Task<GoogleSignInAccount> completedTask = GoogleSignIn.getSignedInAccountFromIntent(data);

            try {
                GoogleSignInAccount account = completedTask.getResult(ApiException.class);
                RegisterRequest registerRequest = new RegisterRequest();
                registerRequest.setEmail(account.getEmail());
                registerRequest.setPassword(null);
                registerRequest.setGender(null);
                registerRequest.setFirstname(account.getGivenName());
                registerRequest.setLastname(account.getFamilyName());
                registerRequest.setGoogleId(account.getId());

                presenter.Register(registerRequest);

            } catch (ApiException e) {
                if(e.getStatusCode() == SIGN_IN_CANCELLED){
                }else if(e.getStatusCode() == SIGN_IN_FAILED){
                    Log.e("Error google",  " " + e.getStatusCode());
                    simpleAlert("Error Google", GoogleSignInStatusCodes.getStatusCodeString(e.getStatusCode()));
                } else{
                    Log.e("Error google",  GoogleSignInStatusCodes.getStatusCodeString(e.getStatusCode()));
                    simpleAlert("Error Google", GoogleSignInStatusCodes.getStatusCodeString(e.getStatusCode()));
                }
            }

            return true;
        }
        return  false;
    }

    private void callbackFacebookSuccess(LoginResult loginResult){
        AccessToken accessToken = loginResult.getAccessToken();

        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        if(response.getError() == null){
                            RegisterRequest registerRequest = new RegisterRequest();
                            try {
                                registerRequest.setFacebookId(object.getString("id"));
                                registerRequest.setEmail(object.getString("email"));
                                registerRequest.setPassword(null);
                                registerRequest.setGender(null);
                                registerRequest.setFirstname(object.getString("first_name"));
                                registerRequest.setLastname(object.getString("last_name"));
                                presenter.Register(registerRequest);
                            } catch (JSONException e) {
                                simpleAlert("Error al obtener los datos de su perfil en facebook");
                            }
                        }else{
                            simpleAlert("Error al consultar los datos de su perfil en facebook");
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,email,last_name,middle_name,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(callbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }

        if (onActivityResultGoogleSignIn(requestCode, data)) {
            return;
        }
    }
}
