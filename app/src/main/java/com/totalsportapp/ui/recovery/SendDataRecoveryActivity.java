package com.totalsportapp.ui.recovery;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.totalsportapp.R;
import com.totalsportapp.data.db.model.LoggedMode;
import com.totalsportapp.data.db.model.Recovery;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.login.LoginActivity;
import com.totalsportapp.ui.start.StartActivity;

public class SendDataRecoveryActivity extends BaseAppCompatActivity implements View.OnClickListener, RecoveryView {

    private final static int RC_SIGN_IN_GOOGLE = 100;
    private IRecoveryPresenter presenter;

    //region declare controls
    private Button btnSendData;
    private EditText etxtEmail;
    private EditText etxtPass;
    private EditText etxtConfirmPass;
    private EditText etxtToken;
    private String email;
    private TextView tvIngresar;

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_pass_recovery);

        onCreateBase();

        findControls();

        configureControls();

        presenter = new RecoveryPresenter(this);
        //setFakeDataTest();
    }


    private void findControls(){
        email = getIntent().getExtras().getString("email");
        btnSendData = findViewById(R.id.btn_recovery_pass);
        etxtEmail = findViewById(R.id.etxt_email);
        etxtPass = findViewById(R.id.etxt_pass);
        etxtConfirmPass = findViewById(R.id.etxt_confirm_pass);
        etxtToken = findViewById(R.id.etxt_token);
        etxtEmail.setText(email);
        tvIngresar = findViewById(R.id.txtv_has_account);
        tvIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ctx, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });

    }

    private void configureControls(){
        btnSendData.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_recovery_pass:
                hasAccountClick();
                break;
            default:
        }
    }

    private void hasAccountClick(){
        if(etxtConfirmPass.getText().toString().equals(etxtPass.getText().toString())){
            Recovery recovery = new Recovery();
            recovery.setClave(etxtPass.getText().toString());
            recovery.setEmail(etxtEmail.getText().toString());
            recovery.setToken(etxtToken.getText().toString());
            presenter.sendDataRecovery(recovery);
        }else{
            simpleAlert("ADVERTENCIA", "La constraseñas no coinciden");
        }
    }

    @Override
    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    @Override
    public void onSendEmailSuccess() {

    }

    @Override
    public void onSendEmailFail(String message) {

    }

    @Override
    public void onSendDataSuccess() {
        Toast.makeText(ctx, "¡Listo, Contraseña cambiada!", Toast.LENGTH_LONG).show();

        Intent i = new Intent(ctx, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);

        overridePendingTransition(R.anim.anim_enter, R.anim.anim_leave);
    }

    @Override
    public void onSendDataFail() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        simpleAlert("No se ha podido actualizar la contraseña");
    }

    @Override
    public void showFormValidateError(String messague) {
        simpleAlert(messague);
    }

    @Override
    public void showProcessingRegister() {
        progressDialog.show();
    }
}
