package com.totalsportapp.ui.recovery;

import java.util.List;

public interface RecoveryView {

    //void saveAuthToken(String accessToken, String refreshToken);

    void onSendEmailSuccess();
    void onSendEmailFail(String message);

    void onSendDataSuccess();
    void onSendDataFail();

    void showFormValidateError(String messague);
    void showProcessingRegister();

    void showInternalError();
}
