package com.totalsportapp.ui.recovery;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.totalsportapp.R;
import com.totalsportapp.data.api.model.RegisterRequest;
import com.totalsportapp.data.db.model.Gender;
import com.totalsportapp.data.db.model.LoggedMode;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.login.LoginActivity;
import com.totalsportapp.ui.main.MainActivity;
import com.totalsportapp.ui.register.IRegisterPresenter;
import com.totalsportapp.ui.register.RegisterPresenter;
import com.totalsportapp.ui.register.RegisterView;
import com.totalsportapp.ui.start.StartActivity;
import com.totalsportapp.util.GoogleSignInUtil;
import com.totalsportapp.util.ValidationUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import static com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes.SIGN_IN_CANCELLED;
import static com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes.SIGN_IN_FAILED;

public class SendEmailRecoveryActivity extends BaseAppCompatActivity implements View.OnClickListener, RecoveryView {

    private final static int RC_SIGN_IN_GOOGLE = 100;
    private IRecoveryPresenter presenter;

    //region declare controls
    private Button btnSendEmail;
    private EditText etxtEmail;
    private TextView tvIngresar;

    //endregion



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_send_email_recovery);

        onCreateBase();

        findControls();

        configureControls();

        presenter = new RecoveryPresenter(this);

        //setFakeDataTest();
    }


    private void findControls(){
        btnSendEmail = findViewById(R.id.btn_send_email);
        etxtEmail = findViewById(R.id.etxt_email);
        tvIngresar = findViewById(R.id.txtv_has_account);
        tvIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ctx, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });
    }

    private void configureControls(){
        btnSendEmail.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_send_email:
                hasAccountClick();
                break;
            default:
        }
    }

    private void hasAccountClick(){

        if(ValidationUtils.isEmailValid(etxtEmail.getText().toString()) == false){
            showFormValidateError("Email no válido");
        }else{
            presenter.sendEmailRecovery(etxtEmail.getText().toString());
        }
    }

    @Override
    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    @Override
    public void onSendEmailSuccess() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        Toast.makeText(ctx, "¡Por favor, revise su bandeja de entrada!", Toast.LENGTH_LONG).show();

        Intent i = new Intent(ctx, SendDataRecoveryActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.putExtra("email", etxtEmail.getText().toString());
        startActivity(i);

        overridePendingTransition(R.anim.anim_enter, R.anim.anim_leave);
    }

    @Override
    public void onSendEmailFail(String message) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }
        simpleAlert(message);
    }

    @Override
    public void onSendDataSuccess() {
        Toast.makeText(ctx, "¡Listo!", Toast.LENGTH_LONG).show();

        Intent i = new Intent(ctx, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);

        overridePendingTransition(R.anim.anim_enter, R.anim.anim_leave);
    }

    @Override
    public void onSendDataFail() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("No se ha podido actualizar la contraseña");
    }

    @Override
    public void showFormValidateError(String messague) {
        simpleAlert(messague);
    }

    @Override
    public void showProcessingRegister() {
            progressDialog.show();
    }
}
