package com.totalsportapp.ui.recovery;

import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.ServicesAPI;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.model.RegisterRequest;
import com.totalsportapp.data.api.model.TokenAPI;
import com.totalsportapp.data.db.model.Recovery;
import com.totalsportapp.ui.register.IRegisterPresenter;
import com.totalsportapp.ui.register.RegisterView;
import com.totalsportapp.util.TokenUtils;
import com.totalsportapp.util.ValidationUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecoveryPresenter implements IRecoveryPresenter {

    private RecoveryView view;

    public RecoveryPresenter(RecoveryView recoveryView){
        view = recoveryView;
    }

    private boolean validateRegister(Recovery recovery){


        if(ValidationUtils.isEmailValid(recovery.getEmail()) == false){
            view.showFormValidateError("Email no válido");
            return false;
        }

        if(recovery.getEmail().isEmpty()){
            view.showFormValidateError("El email es requerido");
            return false;
        }

        if(recovery.getToken().isEmpty()){
            view.showFormValidateError("El token es requerido");
            return false;
        }

        if( recovery.getClave().isEmpty()){
            view.showFormValidateError("La contraseña es requerida");
            return false;
        }

        if(recovery.getClave().length() > 25){
            view.showFormValidateError("Contraseña extensa, máximo 25 dígitos");
            return false;
        }

        return true;
    }


    @Override
    public void sendEmailRecovery(String email) {

        view.showProcessingRegister();

        ServicesAPI servicesAPI = RetrofitClient.getClient().create(ServicesAPI.class);
        Call<OperationResult> call = servicesAPI.sendEmailRecovery(email);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()) {
                    if(response.code() == 200){
                        if(response.body().isStatus()) {
                            view.onSendEmailSuccess();
                        }else {
                            view.onSendEmailFail("No se ha podido enviar un correo, intente nuevamente por favor.");
                        }
                    }
                }else{
                    if(response.code() == 400){
                        view.onSendEmailFail("El correo ingresado no existe");
                    }else{
                        view.showInternalError();
                    }
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                view.showInternalError();
            }
        });
    }

    @Override
    public void sendDataRecovery(Recovery recovery) {


        if(validateRegister(recovery) == false){
            return;
        }

        view.showProcessingRegister();

        ServicesAPI servicesAPI = RetrofitClient.getClient().create(ServicesAPI.class);
        Call<OperationResult> call = servicesAPI.sendDataRecovery(recovery);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()) {
                    if(response.body().isStatus()) {
                        view.onSendDataSuccess();
                    }else {
                        view.onSendDataFail();
                    }
                }else{
                    view.showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                view.showInternalError();
            }
        });
    }

}

