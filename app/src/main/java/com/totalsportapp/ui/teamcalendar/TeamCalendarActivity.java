package com.totalsportapp.ui.teamcalendar;

import android.content.Intent;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.CalendarioEquipoAPI;
import com.totalsportapp.data.api.model.RequestCalendarModel;
import com.totalsportapp.data.api.model.SolicitudEquipoAPI;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.requestchallenge.TeamMembersRequestActivity;
import com.totalsportapp.ui.teammembers.TeamMembersActivity;
import com.totalsportapp.ui.teammemberslocation.TeamMembersMapsActivity;
import com.totalsportapp.util.NetworkUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamCalendarActivity extends BaseAppCompatActivity {

    //region toolbar
    private Toolbar toolbar;
    private ImageView imgv_icon_joined_calendar;
    private ImageView imgv_icon_joined_time;
    private ImageView imgv_icon_joined_place;

    private ImageView imgv_icon_challenged_calendar;
    private ImageView imgv_icon_challenged_time;
    private ImageView imgv_icon_challenged_place;

    private LinearLayout ll_container_joined_team;
    private LinearLayout ll_container_challenged_myteams;

    public final int JOINED = 1;
    public final int CHALLENGED = 2;

    private int tipeList;

    //endreigon

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_calendar);

        onCreateBase();

        findControls();

        configureControls();

        if(NetworkUtils.isNetworkConnected(ctx)){
            loadCalendar();
        }else{
            showDisconnectedNetwork();
            onBackPressed();
        }
    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        imgv_icon_joined_calendar = findViewById(R.id.imgv_icon_joined_calendar);
        imgv_icon_joined_time = findViewById(R.id.imgv_icon_joined_time);
        imgv_icon_joined_place = findViewById(R.id.imgv_icon_joined_place);

        imgv_icon_challenged_calendar = findViewById(R.id.imgv_icon_challenged_calendar);
        imgv_icon_challenged_time = findViewById(R.id.imgv_icon_challenged_time);
        imgv_icon_challenged_place = findViewById(R.id.imgv_icon_challenged_place);

        ll_container_joined_team = findViewById(R.id.ll_container_joined_team);
        ll_container_challenged_myteams = findViewById(R.id.ll_container_challenged_myteams);
    }

    private void configureControls(){
        setUpToolbar();

        imgv_icon_joined_calendar.setVisibility(View.GONE);
        imgv_icon_joined_time.setVisibility(View.GONE);
        imgv_icon_joined_place.setVisibility(View.GONE);

        imgv_icon_challenged_calendar.setVisibility(View.GONE);
        imgv_icon_challenged_time.setVisibility(View.GONE);
        imgv_icon_challenged_place.setVisibility(View.GONE);
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Calendario");
    }

    private void loadCalendar(){
        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<RequestCalendarModel> call = authServicesAPI.calendar(1, 1);
        call.enqueue(new Callback<RequestCalendarModel>() {
            @Override
            public void onResponse(Call<RequestCalendarModel> call, Response<RequestCalendarModel> response) {
                if(response.isSuccessful()){

                    //ArrayList<CalendarioEquipoAPI> data = response.body();

                    ArrayList<CalendarioEquipoAPI> joined = new ArrayList<>();
                    ArrayList<CalendarioEquipoAPI> challenged = new ArrayList<>();

                    for (int i = 0; i <response.body().getCalendarioEquipoCapitan().getListCalendarioCapitan().size() ; i++) {
                        challenged.add(response.body().getCalendarioEquipoCapitan().getListCalendarioCapitan().get(i));
                    }

                    for (int i = 0; i <response.body().getCalendarioEquipoOtros().getListCalendarioEquipoOtros().size() ; i++) {
                        joined.add(response.body().getCalendarioEquipoOtros().getListCalendarioEquipoOtros().get(i));
                    }
                    //data.add(calendarioEquipoAPI);
                    //data.add(newcalendarioEquipoAPI);

                  /*  for(CalendarioEquipoAPI item : data){
                        if(item.getTipoListado() == CalendarioEquipoAPI.CALENDARIO_TIPO_UNIDO){
                            joined.add(item);
                        } else if(item.getTipoListado() == CalendarioEquipoAPI.CALENDARIO_TIPO_RETO){
                            challenged.add(item);
                        }
                    }
*/
                    if(joined.size() == 0){
                        setEmptyLayout(CalendarioEquipoAPI.CALENDARIO_TIPO_UNIDO);
                    }else{
                        imgv_icon_joined_calendar.setVisibility(View.VISIBLE);
                        imgv_icon_joined_time.setVisibility(View.VISIBLE);
                        imgv_icon_joined_place.setVisibility(View.VISIBLE);
                        setLayoutItem(joined);
                    }

                    if(challenged.size() == 0){
                        setEmptyLayout(CalendarioEquipoAPI.CALENDARIO_TIPO_RETO);
                    }else{
                        imgv_icon_challenged_calendar.setVisibility(View.VISIBLE);
                        imgv_icon_challenged_time.setVisibility(View.VISIBLE);
                        imgv_icon_challenged_place.setVisibility(View.VISIBLE);

                        setLayoutItem(challenged);
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<RequestCalendarModel> call, Throwable t) {
                showInternalError();
            }
        });
    }

    private void setEmptyLayout(int typeList){
        View layout_empty = LayoutInflater.from(ctx).inflate(R.layout.layout_empty_calendar, null);
        //TextView txtv_empty_request = layout_empty.findViewById(R.id.txtv_empty_request);
        if(typeList == CalendarioEquipoAPI.CALENDARIO_TIPO_UNIDO){
            //txtv_empty_request.setText(R.string.empty_challenge_request);
            ll_container_joined_team.addView(layout_empty);
        }else if(typeList == CalendarioEquipoAPI.CALENDARIO_TIPO_RETO){
            //txtv_empty_request.setText(R.string.empty_join_request);
            ll_container_challenged_myteams.addView(layout_empty);
        }
    }

    private void setLayoutItem(final ArrayList<CalendarioEquipoAPI> data){
        for(int i = 0; i < data.size(); i++){

            final CalendarioEquipoAPI item = data.get(i);

            View layout = LayoutInflater.from(ctx).inflate(R.layout.layout_calendar_item, null);
            LinearLayout ll_container_item = layout.findViewById(R.id.ll_container_item);
            final TextView txtv_date = layout.findViewById(R.id.txtv_date);
            TextView txtv_hour = layout.findViewById(R.id.txtv_hour);
            TextView txtv_location = layout.findViewById(R.id.txtv_location);
            View vw_separator_a = layout.findViewById(R.id.vw_separator_a);
            View vw_separator_b = layout.findViewById(R.id.vw_separator_b);
            ImageView imgv_icon_arrow = layout.findViewById(R.id.imgv_icon_arrow);

            if(i % 2 == 0){
                ll_container_item.setBackground(ContextCompat.getDrawable(ctx, R.drawable.bg_calendar_item_odd));
                int colorWhite = ContextCompat.getColor(ctx, R.color.white);
                txtv_date.setTextColor(colorWhite);
                txtv_hour.setTextColor(colorWhite);
                txtv_location.setTextColor(colorWhite);
                vw_separator_a.setBackgroundColor(colorWhite);
                vw_separator_b.setBackgroundColor(colorWhite);
                imgv_icon_arrow.setImageResource(R.drawable.calendar_arrow_item_white);
            } else{
                ll_container_item.setBackground(ContextCompat.getDrawable(ctx, R.drawable.bg_calendar_item_even));
                int colorBlack = ContextCompat.getColor(ctx, R.color.black);
                txtv_date.setTextColor(colorBlack);
                txtv_hour.setTextColor(colorBlack);
                txtv_location.setTextColor(colorBlack);
                vw_separator_a.setBackgroundColor(colorBlack);
                vw_separator_b.setBackgroundColor(colorBlack);
                imgv_icon_arrow.setImageResource(R.drawable.calendar_arrow_item_black);
            }

            //layout.setTag(item.getiIdEquipoSolicitudRetar());
            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ctx, TeamMembersMapsActivity.class);
                    i.putExtra(TeamMembersMapsActivity.KEY_TEAM_ID, item.getIdEquipo());
                    i.putExtra(TeamMembersMapsActivity.KEY_CHALLENGE_ID, item.getIdEquipoSolicitudRetar());
                    i.putExtra(TeamMembersMapsActivity.TIPE_LIST, item.getTipoListado());
                    startActivity(i);
                }
            });

            Locale spanish = new Locale("es", "ES");
            txtv_date.setText(item.getFechaReto().format("WWW. DD MMM.", spanish));
            txtv_hour.setText(item.getFechaReto().format("hh12:mm a", spanish));
            txtv_location.setText(item.getUbigeo());

            if(item.getTipoListado() == CalendarioEquipoAPI.CALENDARIO_TIPO_UNIDO){
                ll_container_joined_team.addView(layout);
            } else if(item.getTipoListado() == CalendarioEquipoAPI.CALENDARIO_TIPO_RETO){
                ll_container_challenged_myteams.addView(layout);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }
}
