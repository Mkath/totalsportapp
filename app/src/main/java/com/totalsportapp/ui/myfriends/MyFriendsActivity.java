package com.totalsportapp.ui.myfriends;

import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.miguelcatalan.materialsearchview.MaterialSearchView;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.AmigoAPI;
import com.totalsportapp.data.api.model.Friend;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.db.model.Usuario;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.util.NetworkUtils;
import com.totalsportapp.util.ValidationUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.totalsportapp.util.AppConstants.BASE_URL_PHOTO;

public class MyFriendsActivity extends BaseAppCompatActivity implements View.OnClickListener, SearchFriendInterface {

    //region controls
    private Toolbar toolbar;
    private RelativeLayout rl_container_zero_friends;
    private ListView lv_my_friends;
    private ListView rl_container_found_friend;
    private MaterialSearchView searchView;
    //endregion
    ArrayList<AmigoAPI> requestAmigos;
    ArrayList<AmigoAPI> amigos;

    private final int ACEPTAR_AMIGO = 1;
    private final int RECHAZAR_AMIGO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_friends);

        onCreateBase();

        findControls();

        configureControls();

        requestAmigos = new ArrayList<>();
        amigos = new ArrayList<>();

        if(NetworkUtils.isNetworkConnected(ctx)){
            displaySolicitudFriends();
           // displayMyFriends();
        }else{
            showDisconnectedNetwork();
            onBackPressed();
        }
    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        lv_my_friends = findViewById(R.id.lv_my_friends);
        rl_container_zero_friends = findViewById(R.id.rl_container_zero_friends);
        rl_container_found_friend = findViewById(R.id.container_found_friend);
        searchView = findViewById(R.id.search_view);
    }

    private void configureControls(){
        setUpToolbar();
        setUpSearchView();
    }

    private void setUpSearchView(){
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                sendServerSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
            @Override
            public void onSearchViewShown() {
            }

            @Override
            public void onSearchViewClosed() {
                ListAdapter adapter = lv_my_friends.getAdapter();
                if(adapter != null){
                    if(adapter.getCount() > 0){
                        rl_container_found_friend.setVisibility(View.GONE);
                        lv_my_friends.setVisibility(View.VISIBLE);
                        rl_container_zero_friends.setVisibility(View.GONE);
                    }else{
                        rl_container_found_friend.setVisibility(View.GONE);
                        lv_my_friends.setVisibility(View.GONE);
                        rl_container_zero_friends.setVisibility(View.VISIBLE);
                    }
                }else{
                    rl_container_found_friend.setVisibility(View.GONE);
                    lv_my_friends.setVisibility(View.GONE);
                    rl_container_zero_friends.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Mis Amigos");
    }

    private void displayMyFriends(){
        progressDialog.show();

        AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<AmigoAPI>> call = services.myFriends();
        call.enqueue(new Callback<ArrayList<AmigoAPI>>() {
            @Override
            public void onResponse(Call<ArrayList<AmigoAPI>> call, Response<ArrayList<AmigoAPI>> response) {
                if (response.isSuccessful()) {
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }


                    if(requestAmigos.size()!= 0){
                        amigos = requestAmigos;
                        for (int i = 0; i <response.body().size() ; i++) {
                            amigos.add(response.body().get(i));
                        }
                    }else {
                        amigos = response.body();
                    }

                    if(amigos.size() == 0){
                        lv_my_friends.setVisibility(View.GONE);
                        rl_container_zero_friends.setVisibility(View.VISIBLE);
                        return;
                    }else{
                        lv_my_friends.setVisibility(View.VISIBLE);
                        rl_container_zero_friends.setVisibility(View.GONE);
                    }

                    if(lv_my_friends.getAdapter() != null){
                        FriendAdapter adapter = (FriendAdapter)lv_my_friends.getAdapter();
                        adapter.clear();
                        adapter.addAll(amigos);
                    } else {
                        getAdapterFriends(amigos);
                    }
                } else {
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<AmigoAPI>> call, Throwable t) {
                showInternalError();
            }
        });
    }
    private void getAdapterFriends(ArrayList<AmigoAPI> list){
        FriendAdapter friendAdapter = new FriendAdapter(ctx, list, this);
        lv_my_friends.setAdapter(friendAdapter);
    }

    private void displaySolicitudFriends(){
        progressDialog.show();

        AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<AmigoAPI>> call = services.myRequestFriends();
        call.enqueue(new Callback<ArrayList<AmigoAPI>>() {
            @Override
            public void onResponse(Call<ArrayList<AmigoAPI>> call, Response<ArrayList<AmigoAPI>> response) {
                if (response.isSuccessful()) {
                    /*if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }*/

                    requestAmigos = response.body();
                    displayMyFriends();

                } else {
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<AmigoAPI>> call, Throwable t) {
                showInternalError();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_my_friends, menu);

        MenuItem searchItem = menu.findItem(R.id.add_friend_action);
        searchView.setMenuItem(searchItem);

        return true;
    }

    private void sendServerSearch(String nombreAlias){

        if(PreferencesHelper.getInstance().getUser().getEmail().equalsIgnoreCase(nombreAlias)){
            simpleAlert("No puede autoagregarse");
            return;
        }

        progressDialog.show();

        AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<Friend>> call = services.searchFriend(nombreAlias);
        call.enqueue(new Callback<ArrayList<Friend>>() {
            @Override
            public void onResponse(Call<ArrayList<Friend>> call, Response<ArrayList<Friend>> response) {
                if (response.isSuccessful()) {

                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }

                    //rl_container_found_friend.removeAllViews();
                    lv_my_friends.setVisibility(View.GONE);
                    rl_container_zero_friends.setVisibility(View.GONE);
                    rl_container_found_friend.setVisibility(View.VISIBLE);

                    if(response.body() == null){
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

                        TextView notFoundFriend = new TextView(ctx);
                        notFoundFriend.setText(R.string.user_not_exist);
                        notFoundFriend.setTextSize(TypedValue.COMPLEX_UNIT_SP,17);
                        notFoundFriend.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                        notFoundFriend.setLayoutParams(layoutParams);
                        notFoundFriend.setTextColor(ResourcesCompat.getColor(getResources(), R.color.txtv_empty_list, ctx.getTheme()));
                        rl_container_found_friend.addView(notFoundFriend);

                    }else{
                        if(rl_container_found_friend.getAdapter() != null){
                            SearchFriendAdapter adapter = (SearchFriendAdapter)rl_container_found_friend.getAdapter();
                            adapter.clear();
                            adapter.addAll(response.body());
                        } else {
                           getAdapter(response.body());
                        }
                    }

                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Friend>> call, Throwable t) {
                showInternalError();
            }
        });
    }
    private void getAdapter(ArrayList<Friend> list){
        SearchFriendAdapter friendAdapter = new SearchFriendAdapter(ctx, list, this);
        rl_container_found_friend.setAdapter(friendAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    public void showInternalErrorMessage(String message) {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", message);

    }
        public void btnAddFriendClick(final int idUsuarioAmigo){
        confirmAlert("¿Agregar?", new ExecuteParam() {
            @Override
            public void onExecute() {
                progressDialog.show();

                AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
                Call<OperationResult> call = services.addFriend(idUsuarioAmigo);
                call.enqueue(new Callback<OperationResult>() {
                    @Override
                    public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                        if (response.isSuccessful()) {
                            OperationResult opResult = response.body();
                            if(opResult.isStatus()){
                                rl_container_found_friend.setVisibility(View.GONE);
                                if (searchView.isSearchOpen()) {
                                    searchView.closeSearch();
                                }
                                Toast.makeText(ctx, "Invitación Enviada", Toast.LENGTH_LONG).show();
                                displaySolicitudFriends();
                               // displayMyFriends();
                                //setList(requestAmigos,amigos);
                            }else{
                                showResultAddFriendFail(opResult.getErrors());
                            }
                        }else{
                            showInternalError();
                        }
                    }

                    @Override
                    public void onFailure(Call<OperationResult> call, Throwable t) {
                        showInternalError();
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    public void showResultAddFriendFail(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("Error", errors.get(0));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_add_friend:
                btnAddFriendClick((int)view.getTag());
                break;
        }
    }

    @Override
    public void clickAdd(int idAmigo) {
        btnAddFriendClick(idAmigo);
    }

    @Override
    public void clickAccept(final int idSolicitud) {
        confirmAlert("¿Aceptar amigo?", new ExecuteParam() {
            @Override
            public void onExecute() {
                AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
                Call<OperationResult> call = services.sendResponseFriends(idSolicitud, ACEPTAR_AMIGO);
                call.enqueue(new Callback<OperationResult>() {
                    @Override
                    public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                        if (response.isSuccessful()) {
                            OperationResult opResult = response.body();
                            if(opResult.isStatus()){
                                displaySolicitudFriends();
                            }else{
                                showResultAddFriendFail(opResult.getErrors());
                            }
                        }else{
                            showInternalError();
                        }
                    }

                    @Override
                    public void onFailure(Call<OperationResult> call, Throwable t) {
                        showInternalError();
                    }
                });

            }
        });
    }

    @Override
    public void clickDeny(final int idSolicitud) {
        confirmAlert("¿Rechazar amigo?", new ExecuteParam() {
            @Override
            public void onExecute() {
                AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
                Call<OperationResult> call = services.sendResponseFriends(idSolicitud, RECHAZAR_AMIGO);
                call.enqueue(new Callback<OperationResult>() {
                    @Override
                    public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                        if (response.isSuccessful()) {
                            OperationResult opResult = response.body();
                            if(opResult.isStatus()){
                                displaySolicitudFriends();
                            }else{
                                showResultAddFriendFail(opResult.getErrors());
                            }
                        }else{
                            showInternalError();
                        }
                    }

                    @Override
                    public void onFailure(Call<OperationResult> call, Throwable t) {
                        showInternalError();
                    }
                });

            }
        });

    }

    @Override
    public void clickDelete(final int idFriendDelete) {
        confirmAlert("¿Desea eliminar a su amigo?", new ExecuteParam() {
            @Override
            public void onExecute() {
                AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
                Call<OperationResult> call = services.sendDeleteFriend(idFriendDelete);
                call.enqueue(new Callback<OperationResult>() {
                    @Override
                    public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                        if (response.isSuccessful()) {
                            OperationResult opResult = response.body();
                            if(opResult.isStatus()){
                                displaySolicitudFriends();
                            }else{
                                showResultAddFriendFail(opResult.getErrors());
                            }
                        }else{
                            showInternalError();
                        }
                    }

                    @Override
                    public void onFailure(Call<OperationResult> call, Throwable t) {
                        showInternalError();
                    }
                });

            }
        });
    }
}
