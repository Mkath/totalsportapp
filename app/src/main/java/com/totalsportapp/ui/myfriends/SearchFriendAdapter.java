package com.totalsportapp.ui.myfriends;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.AmigoAPI;
import com.totalsportapp.data.api.model.Friend;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.db.model.EstadoAmigo;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.util.GlideApp;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFriendAdapter extends ArrayAdapter<Friend> {
    private Context ctx;
    SearchFriendInterface friendInterface;
    public SearchFriendAdapter(Context context, ArrayList<Friend> items , SearchFriendInterface friendInterface) {
        super(context, 0, items);
        this.ctx = context;
        this.friendInterface = friendInterface;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_item_friend, parent, false);
        }

        final Friend friend = getItem(position);

        ImageView imgv_friend_photo = convertView.findViewById(R.id.imgv_friend_photo);
        TextView txtv_friend_name = convertView.findViewById(R.id.txtv_friend_name);
        TextView txtv_friend_nickname = convertView.findViewById(R.id.txtv_friend_nickname);
        Button btn_add_friend = convertView.findViewById(R.id.btn_add_friend);

        if(friend.getFoto() != null){
            imgv_friend_photo.setPadding(0,0,0,0);
            String photoUrl =  friend.getFoto();
            //have to build project if GlideApp is not found
            Glide.with(getContext())
                    .load(photoUrl)
                    .dontAnimate()
                    .centerCrop()
                    .into(imgv_friend_photo);
        }else{
            GlideApp.with(getContext())
                    .load(getContext().getResources().getDrawable(R.drawable.logo_total_sport_png_512_x_512))
                    .into(imgv_friend_photo);
        }

        txtv_friend_name.setText(friend.getNombreCompleto());

        if(friend.getAlias() != null){
            txtv_friend_nickname.setText(friend.getAlias());
        }else{
            txtv_friend_nickname.setText("Sin allias");
        }

        btn_add_friend.setVisibility(View.VISIBLE);
        //btn_add_friend.setOnClickListener(MyFriendsActivity.this);
        btn_add_friend.setTag(friend.getIdUsuario());

        btn_add_friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                friendInterface.clickAdd(friend.getIdUsuario());
            }
        });

        return convertView;
    }

}
