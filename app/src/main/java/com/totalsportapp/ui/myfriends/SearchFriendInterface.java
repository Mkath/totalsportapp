package com.totalsportapp.ui.myfriends;

public interface SearchFriendInterface {
    void clickAdd(int idAmigo);

    void clickAccept(int idSolicitud);
    void clickDeny(int idSolicitud);
    void clickDelete(int idFriendDelete);

}
