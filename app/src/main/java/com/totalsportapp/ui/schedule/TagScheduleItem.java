package com.totalsportapp.ui.schedule;

public class TagScheduleItem {

    private boolean selected;
    private int day;
    private int hour;

    public TagScheduleItem(){
    }

    public TagScheduleItem(boolean selected, int day, int hour) {
        this.selected = selected;
        this.day = day;
        this.hour = hour;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }
}
