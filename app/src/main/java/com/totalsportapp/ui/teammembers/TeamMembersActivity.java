package com.totalsportapp.ui.teammembers;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.EquipoMiembroAPI;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.util.NetworkUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamMembersActivity extends BaseAppCompatActivity {


    public static final String KEY_TEAM_ID = "KEY_TEAM_ID";
    //region controls
    private Toolbar toolbar;
    private ListView lv_team_members;
    private RelativeLayout rl_container_zero_members;
    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_members);

        onCreateBase();

        findControls();

        configureControls();

        if(NetworkUtils.isNetworkConnected(ctx)){
            displayFriends(this);
        }else{
            showDisconnectedNetwork();
            onBackPressed();
        }
    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        lv_team_members = findViewById(R.id.lv_team_members);
        rl_container_zero_members = findViewById(R.id.rl_container_zero_members);
    }

    private void configureControls(){
        setUpToolbar();
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Miembros");
    }

    private void displayFriends(final Activity activity){
        progressDialog.show();

        int idEquipo = getIntent().getExtras().getInt(KEY_TEAM_ID);

        AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<EquipoMiembroAPI>> call = services.teamMembers(idEquipo);
        call.enqueue(new Callback<ArrayList<EquipoMiembroAPI>>() {
            @Override
            public void onResponse(Call<ArrayList<EquipoMiembroAPI>> call, Response<ArrayList<EquipoMiembroAPI>> response) {
                if (response.isSuccessful()) {
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }

                    ArrayList<EquipoMiembroAPI> equipoMiembroAPI = response.body();

                    if(equipoMiembroAPI.size() == 0){
                        lv_team_members.setVisibility(View.GONE);
                        rl_container_zero_members.setVisibility(View.VISIBLE);
                        return;
                    }else{
                        lv_team_members.setVisibility(View.VISIBLE);
                        rl_container_zero_members.setVisibility(View.GONE);
                    }

                    TeamMembersAdapter teamMembersAdapter = new TeamMembersAdapter(ctx, equipoMiembroAPI, activity);
                    lv_team_members.setAdapter(teamMembersAdapter);
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<EquipoMiembroAPI>> call, Throwable t) {
                showInternalError();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showInternalError(){
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }
}
