package com.totalsportapp.ui.teammembers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.totalsportapp.R;
import com.totalsportapp.data.api.model.AmigoAPI;
import com.totalsportapp.data.api.model.EquipoMiembroAPI;
import com.totalsportapp.ui.profile.ProfileActivity;
import com.totalsportapp.util.GlideApp;

import java.util.ArrayList;

import static com.totalsportapp.util.AppConstants.BASE_URL_PHOTO;

public class TeamMembersAdapter extends ArrayAdapter<EquipoMiembroAPI> {

    private Activity mActivity;

    public TeamMembersAdapter(Context context, ArrayList<EquipoMiembroAPI> items, Activity activity) {
        super(context, 0, items);
        this.mActivity = activity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_item_team_members, parent, false);
        }

        final EquipoMiembroAPI amigoAPI = getItem(position);
        ImageView imgv_friend_photo = convertView.findViewById(R.id.imgv_friend_photo);
        TextView txtv_friend_name = convertView.findViewById(R.id.txtv_friend_name);
        TextView txtv_friend_nickname = convertView.findViewById(R.id.txtv_friend_nickname);
        RelativeLayout container = convertView.findViewById(R.id.container_member);

        if(amigoAPI.getFoto() != null){
            imgv_friend_photo.setPadding(0,0,0,0);
            String photoUrl =  amigoAPI.getFoto();
            //have to build project if GlideApp is not found
            Glide.with(getContext())
                    .load(photoUrl)
                    .dontAnimate()
                    .centerCrop()
                    .into(imgv_friend_photo);
        }else{
            GlideApp.with(getContext())
                    .load(getContext().getResources().getDrawable(R.drawable.logo_total_sport_png_512_x_512))
                    .into(imgv_friend_photo);

        }

        txtv_friend_name.setText(amigoAPI.getNombreCompleto());
        if(amigoAPI.getAlias() != null){
            txtv_friend_nickname.setText(amigoAPI.getAlias());
        }else{
            txtv_friend_nickname.setText("Sin alias");

        }

        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, ProfileActivity.class);
                intent.putExtra("amigoAPI", amigoAPI);
                mActivity.startActivity(intent);
            }
        });

        return convertView;
    }
}
