package com.totalsportapp.ui.teammembers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalsportapp.R;
import com.totalsportapp.data.api.model.AmigoAPI;
import com.totalsportapp.data.db.model.Usuario;
import com.totalsportapp.util.GlideApp;

import java.util.ArrayList;

import static com.totalsportapp.util.AppConstants.BASE_URL_PHOTO;

public class ChooseTeamMembersAdapter extends ArrayAdapter<ChooseTeamMemberItem> {

    public ChooseTeamMembersAdapter(Context context, ArrayList<ChooseTeamMemberItem> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_item_choose_members, parent, false);
        }

        ChooseTeamMemberItem chooseTeamMemberItem = getItem(position);
        AmigoAPI amigoAPI = chooseTeamMemberItem.getAmigoAPI();
        ImageView imgv_friend_photo = convertView.findViewById(R.id.imgv_friend_photo);
        TextView txtv_friend_name = convertView.findViewById(R.id.txtv_friend_name);
        TextView txtv_friend_nickname = convertView.findViewById(R.id.txtv_friend_nickname);

        if(amigoAPI.getFoto() != null){
            imgv_friend_photo.setPadding(0,0,0,0);
            String photoUrl = amigoAPI.getFoto();
            //have to build project if GlideApp is not found
            GlideApp.with(getContext())
                    .load(photoUrl)
                    .dontAnimate()
                    .centerCrop()
                    .into(imgv_friend_photo);
        }

        txtv_friend_name.setText(amigoAPI.getNombreCompleto());
        if(amigoAPI.getAlias() != null){
            txtv_friend_nickname.setText(amigoAPI.getAlias());
        }

        if(chooseTeamMemberItem.isSelected() == false){
            convertView.setTag("0");
        }else{
            View vw_selecct = convertView.findViewById(R.id.vw_selecct);
            vw_selecct.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.bg_choose_members_select));
            convertView.setTag("1");
        }

        return convertView;
    }
}
