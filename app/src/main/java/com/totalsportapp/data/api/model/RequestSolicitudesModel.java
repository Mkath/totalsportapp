package com.totalsportapp.data.api.model;

public class RequestSolicitudesModel {
    private ListaUnirData listaUnirData;
    private ListaRetosData listaRetosData;

    public ListaUnirData getListaUnirData() {
        return listaUnirData;
    }

    public void setListaUnirData(ListaUnirData listaUnirData) {
        this.listaUnirData = listaUnirData;
    }

    public ListaRetosData getListaRetosData() {
        return listaRetosData;
    }

    public void setListaRetosData(ListaRetosData listaRetosData) {
        this.listaRetosData = listaRetosData;
    }
}
