package com.totalsportapp.data.api;

import android.util.Log;

import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.util.AppConstants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.totalsportapp.data.api.UtilAPI.getGsonSync;

public class RetrofitClient {

    private static Retrofit retrofit = null;
    private static Retrofit retrofitAuth = null;

    public static void clearAuthClient(){
        retrofitAuth = null;
    }

    public static Retrofit getClientAuth() {

        //todo borrar
        PreferencesHelper prefas = PreferencesHelper.getInstance();
        String accessTokena = prefas.getAccessToken();
        Log.d("ERRORAR", "Current" + accessTokena);

        if (retrofitAuth == null) {

            PreferencesHelper prefs = PreferencesHelper.getInstance();
            final String accessToken = prefs.getAccessToken();
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.connectTimeout(60, TimeUnit.SECONDS);
            httpClient.readTimeout(60, TimeUnit.SECONDS);
            httpClient.writeTimeout(60, TimeUnit.SECONDS);
            httpClient.authenticator(new TokenAuthenticator());
            httpClient.addInterceptor(new Interceptor() {
                  @Override
                  public Response intercept(Interceptor.Chain chain) throws IOException {
                      Request original = chain.request();

                      //todo borrar
                      Log.d("ERRORAR", "Sended" + accessToken);

                      Request request = original.newBuilder()
                              .header("Authorization", "Bearer " + accessToken)
                              .method(original.method(), original.body())
                              .build();

                      return chain.proceed(request);
                  }
            });
            httpClient.addInterceptor(logging);

            OkHttpClient client = httpClient.build();

            retrofitAuth = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL_API)
                    .addConverterFactory(GsonConverterFactory.create(getGsonSync()))
                    .client(client)
                    .build();
        }
        return retrofitAuth;
    }

    public static Retrofit getClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        if (retrofit == null) {
            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(logging);
            OkHttpClient client = httpClient.build();
            retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstants.BASE_URL_API)
                    .addConverterFactory(GsonConverterFactory.create(getGsonSync()))
                    .client(client)
                    .build();
        }
        return retrofit;
    }
}
