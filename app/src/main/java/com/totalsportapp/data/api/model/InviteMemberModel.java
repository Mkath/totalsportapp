package com.totalsportapp.data.api.model;

import java.io.Serializable;

public class InviteMemberModel implements Serializable {
    private int IdEquipoMiembro;
    private int IdEquipoSolicitudRetar;

    public int getIdEquipoMiembro() {
        return IdEquipoMiembro;
    }

    public void setIdEquipoMiembro(int idEquipoMiembro) {
        IdEquipoMiembro = idEquipoMiembro;
    }

    public int getIdEquipoSolicitudRetar() {
        return IdEquipoSolicitudRetar;
    }

    public void setIdEquipoSolicitudRetar(int idEquipoSolicitudRetar) {
        IdEquipoSolicitudRetar = idEquipoSolicitudRetar;
    }
}
