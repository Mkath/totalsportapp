package com.totalsportapp.data.api.model;

import java.util.ArrayList;

import hirondelle.date4j.DateTime;

public class EquipoSolicitudRetarDataFCM {

    private ArrayList<String> emailsRetadores;
    private ArrayList<String> emailsRetados;
    private DateTime fechaReto;
    private int idEquipoSolicitudRetar;
    private int equipoRetado;
    private int equipoRetador;
    private int MiEquipo;

    public int getMiEquipo() {
        return MiEquipo;
    }

    public void setMiEquipo(int miEquipo) {
        MiEquipo = miEquipo;
    }

    public ArrayList<String> getEmailsRetadores() {
        return emailsRetadores;
    }

    public void setEmailsRetadores(ArrayList<String> emailsRetadores) {
        this.emailsRetadores = emailsRetadores;
    }

    public ArrayList<String> getEmailsRetados() {
        return emailsRetados;
    }

    public void setEmailsRetados(ArrayList<String> emailsRetados) {
        this.emailsRetados = emailsRetados;
    }

    public DateTime getFechaReto() {
        return fechaReto;
    }

    public void setFechaReto(DateTime fechaReto) {
        this.fechaReto = fechaReto;
    }

    public int getIdEquipoSolicitudRetar() {
        return idEquipoSolicitudRetar;
    }

    public void setIdEquipoSolicitudRetar(int idEquipoSolicitudRetar) {
        this.idEquipoSolicitudRetar = idEquipoSolicitudRetar;
    }

    public int getEquipoRetado() {
        return equipoRetado;
    }

    public void setEquipoRetado(int equipoRetado) {
        this.equipoRetado = equipoRetado;
    }

    public int getEquipoRetador() {
        return equipoRetador;
    }

    public void setEquipoRetador(int equipoRetador) {
        this.equipoRetador = equipoRetador;
    }
}
