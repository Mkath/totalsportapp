package com.totalsportapp.data.api.model;

public class RequestCalendarModel {
    private CalendarioEquipoCapitan calendarioEquipoCapitan;
    private CalendarioEquipoOtros calendarioEquipoOtros;

    public CalendarioEquipoCapitan getCalendarioEquipoCapitan() {
        return calendarioEquipoCapitan;
    }

    public void setCalendarioEquipoCapitan(CalendarioEquipoCapitan calendarioEquipoCapitan) {
        this.calendarioEquipoCapitan = calendarioEquipoCapitan;
    }

    public CalendarioEquipoOtros getCalendarioEquipoOtros() {
        return calendarioEquipoOtros;
    }

    public void setCalendarioEquipoOtros(CalendarioEquipoOtros calendarioEquipoOtros) {
        this.calendarioEquipoOtros = calendarioEquipoOtros;
    }
}
