package com.totalsportapp.data.api.model;

import java.io.Serializable;

public class MiembroParticipante implements Serializable {

    private int IdMiembroInvitacion;
    private int IdUsuario;
    private String UsuarioNombre;
    private String UsuarioApellido;
    private int EsCapitan;
    private int Estado;
    private String Email;
    private String Foto;
    private String Alias;
    private String Posicion;
    private int IdEquipoMiembro;

    public int getIdEquipoMiembro() {
        return IdEquipoMiembro;
    }

    public void setIdEquipoMiembro(int idEquipoMiembro) {
        IdEquipoMiembro = idEquipoMiembro;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getFoto() {
        return Foto;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public String getAlias() {
        return Alias;
    }

    public void setAlias(String alias) {
        Alias = alias;
    }

    public String getPosicion() {
        return Posicion;
    }

    public void setPosicion(String posicion) {
        Posicion = posicion;
    }

    public int getEstado() {
        return Estado;
    }

    public void setEstado(int estado) {
        Estado = estado;
    }

    public int getIdMiembroInvitacion() {
        return IdMiembroInvitacion;
    }

    public void setIdMiembroInvitacion(int idMiembroInvitacion) {
        IdMiembroInvitacion = idMiembroInvitacion;
    }

    public int getIdUsuario() {
        return IdUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        IdUsuario = idUsuario;
    }

    public String getUsuarioNombre() {
        return UsuarioNombre;
    }

    public void setUsuarioNombre(String usuarioNombre) {
        UsuarioNombre = usuarioNombre;
    }

    public String getUsuarioApellido() {
        return UsuarioApellido;
    }

    public void setUsuarioApellido(String usuarioApellido) {
        UsuarioApellido = usuarioApellido;
    }

    public int getEsCapitan() {
        return EsCapitan;
    }

    public void setEsCapitan(int esCapitan) {
        EsCapitan = esCapitan;
    }
}
