package com.totalsportapp.data.api;

import com.totalsportapp.data.api.model.AmigoAPI;
import com.totalsportapp.data.api.model.CalendarioEquipoAPI;
import com.totalsportapp.data.api.model.EquipoMiembroAPI;
import com.totalsportapp.data.api.model.EquipoSolicitudRetarRequest;
import com.totalsportapp.data.api.model.EquipoSolicitudUnirRequest;
import com.totalsportapp.data.api.model.FilterTeamAppRequest;
import com.totalsportapp.data.api.model.Friend;
import com.totalsportapp.data.api.model.InvitacionReto;
import com.totalsportapp.data.api.model.InviteMemberModel;
import com.totalsportapp.data.api.model.InviteMemberResponse;
import com.totalsportapp.data.api.model.LogoutModel;
import com.totalsportapp.data.api.model.MiembroParticipante;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.model.QualifyChallengeModel;
import com.totalsportapp.data.api.model.QualifyMemberModel;
import com.totalsportapp.data.api.model.RequestCalendarModel;
import com.totalsportapp.data.api.model.RequestModel;
import com.totalsportapp.data.api.model.RequestSolicitudesModel;
import com.totalsportapp.data.api.model.ScheduleAPI;
import com.totalsportapp.data.api.model.SolicitudEquipoAPI;
import com.totalsportapp.data.db.model.Equipo;
import com.totalsportapp.data.db.model.Ubigeo;
import com.totalsportapp.data.db.model.Usuario;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface AuthServicesAPI {

    @GET("account/profile")
    Call<Usuario> profile();

    @GET("account/userProfile")
    Call<Usuario> userProfile(@Query("userId") int userId);

    @Multipart
    @POST("account/savePhotoProfile")
    Call<OperationResult> savePhotoProfile(@Part MultipartBody.Part photoProfile);

    @POST("account/saveProfile")
    Call<OperationResult> saveProfile(@Body Usuario usuario);

    @POST("friend/search")
    Call<ArrayList<Friend>> searchFriend(@Query("nombreAlias") String nombreAlias);

    @POST("friend/add")
    Call<OperationResult> addFriend(@Query("idUsuarioAmigo") int idUsuarioAmigo);

    @GET("friend/myFriends")
    Call<ArrayList<AmigoAPI>> myFriends();

    @GET("friend/allrequest")
    Call<ArrayList<AmigoAPI>> myRequestFriends();

    @POST("friend/answerrequest")
    Call<OperationResult> sendResponseFriends(@Query("idRequest")int idRequest,
                                              @Query("Response") int response);


    @POST("friend/Delete")
    Call<OperationResult> sendDeleteFriend(@Query("idFriendDelete")int idFriendDelete);

    @GET("schedule/me")
    Call<ArrayList<ScheduleAPI>> getMySchedule();

    @POST("schedule/save")
    Call<OperationResult> saveSchedule(@Body ArrayList<ScheduleAPI> preferenciaHorarias);

    @GET("account/ubigeo")
    Call<ArrayList<Ubigeo>> getUbigeo();

    @GET("account/Departments")
    Call<ArrayList<Ubigeo>> getDepartamentos();

    @GET("account/ProvincesxId")
    Call<ArrayList<Ubigeo>> getProvincias(@Query("id")String idDepartamento);

    @GET("account/DistrictsxId")
    Call<ArrayList<Ubigeo>> getDistritos(@Query("id")String idProvincia);


    @Multipart
    @POST("team/create")
    Call<OperationResult> createTeam(@Part MultipartBody.Part photoTeam, @Part("teamJSON") RequestBody teamJSON);

    @Multipart
    @POST("team/update")
    Call<OperationResult> updateTeam(@Part MultipartBody.Part photoTeam, @Part("teamJSON") RequestBody teamJSON);

    @GET("team/myTeams")
    Call<ArrayList<Equipo>> myTeams();

    @GET("team/infoTeam")
    Call<Equipo> infoTeam(@Query("idEquipo") int idEquipo);

    @GET("team/teamMembers")
    Call<ArrayList<EquipoMiembroAPI>> teamMembers(@Query("idEquipo") int idEquipo);

    @POST("team/filter")
    Call<RequestModel> filterTeams(@Body FilterTeamAppRequest filterTeamAppRequest);

    @POST("team/joinToTeam")
    Call<OperationResult> joinToTeam(@Body EquipoSolicitudUnirRequest equipoSolicitudUnirRequest);

    @POST("team/challengeTeam")
    Call<OperationResult> challengeTeam(@Body EquipoSolicitudRetarRequest equipoSolicitudRetarRequest);

    @GET("team/teamrequests")
    Call<RequestSolicitudesModel> teamRequests(@Query("pagSolIntegrar") int pagSolIntegrar,
                                               @Query("pagSolRetos") int pagSolRetos);

    @POST("team/responseRequests")
    Call<OperationResult> responseRequests(@Query("idSolicitud") int idSolicitud,
                                           @Query("tipoSolicitud") int tipoSolicitud,
                                           @Query("aprueba") int aprueba);

    @GET("team/MyCaptainTeams")
    Call<ArrayList<Equipo>> myCapitanTeams();

    @GET("team/calendar")
    Call<RequestCalendarModel> calendar(@Query("pagCalCap") int pagCalCap,
                                        @Query("pagCalEqOtros") int pagCalEqOtros);

    @POST("challenge/InviteMember")
    Call<OperationResult> challengeInviteMember(@Body InviteMemberModel inviteMemberModel);

    @POST("challenge/InvitationResponse")
    Call<OperationResult> challengeInviteResponse(@Body InviteMemberResponse inviteMemberResponse);


    @GET("challenge/listparticipants")
    Call<ArrayList<MiembroParticipante>> teamParticipant(@Query("idEquipo") int idEquipo,
                                                         @Query("idReto") int idReto);

    @GET("challenge/ListTeamMembers")
    Call<ArrayList<MiembroParticipante>> teamMembersByReto(@Query("idEquipo") int idEquipo,
                                                           @Query("idReto") int idReto);

    @GET("challenge/ListChallengeRequest")
    Call<ArrayList<InvitacionReto>> listChallengeRequest();

    @POST("challenge/QualifyTeamParticipants")
    Call<OperationResult> challengeQualifyMembers(@Body QualifyMemberModel qualifyMemberModel);

    @POST("challenge/QualifyOpossingTeam")
    Call<OperationResult> challengeQualifyTeam(@Body QualifyChallengeModel qualifyChallengeModel);

    @POST("account/saveTokenFCM")
    Call<ResponseBody> saveTokenFCM(@Query("token") String token);

    @POST("account/refreshTokenFCM")
    Call<ResponseBody> refreshTokenFCM(@Query("oldToken") String oldToken, @Query("newToken") String newToken);
}
