package com.totalsportapp.data.api;

import com.totalsportapp.data.api.model.LoginRequest;
import com.totalsportapp.data.api.model.LogoutModel;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.model.TokenAPI;
import com.totalsportapp.data.api.model.RegisterRequest;
import com.totalsportapp.data.db.model.Recovery;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServicesAPI {

    @POST("account/signIn")
    Call<OperationResult> signIn(@Body RegisterRequest registerRequest);

    @POST("account/logIn")
    Call<TokenAPI> logIn(@Body LoginRequest registerRequest);

    @POST("account/logout")
    Call<ResponseBody> logout(@Body LogoutModel logoutModel);


    @POST("account/generartokenclave")
    Call<OperationResult> sendEmailRecovery(@Query("Email") String email);


    @POST("account/cambiarclave")
    Call<OperationResult> sendDataRecovery(@Body Recovery recovery);
}
