package com.totalsportapp.data.api.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ListaRetosData implements Serializable {
    private int TotalPaginas;
    private ArrayList<SolicitudEquipoAPI> ListRetos;

    public int getTotalPaginas() {
        return TotalPaginas;
    }

    public void setTotalPaginas(int totalPaginas) {
        TotalPaginas = totalPaginas;
    }

    public ArrayList<SolicitudEquipoAPI> getListRetos() {
        return ListRetos;
    }

    public void setListRetos(ArrayList<SolicitudEquipoAPI> listRetos) {
        ListRetos = listRetos;
    }
}
