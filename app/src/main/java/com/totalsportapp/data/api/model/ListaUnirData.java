package com.totalsportapp.data.api.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ListaUnirData implements Serializable {
    private int TotalPaginas;
    private ArrayList<SolicitudEquipoAPI> ListUnir;

    public int getTotalPaginas() {
        return TotalPaginas;
    }

    public void setTotalPaginas(int totalPaginas) {
        TotalPaginas = totalPaginas;
    }

    public ArrayList<SolicitudEquipoAPI> getListUnir() {
        return ListUnir;
    }

    public void setListUnir(ArrayList<SolicitudEquipoAPI> listUnir) {
        ListUnir = listUnir;
    }
}
