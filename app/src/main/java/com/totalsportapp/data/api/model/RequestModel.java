package com.totalsportapp.data.api.model;

import com.totalsportapp.data.db.model.Equipo;

import java.io.Serializable;
import java.util.ArrayList;

public class RequestModel implements Serializable {
    private int TotalPagina;
    private ArrayList<Equipo> Equipos;

    public int getTotalPagina() {
        return TotalPagina;
    }

    public void setTotalPagina(int totalPagina) {
        TotalPagina = totalPagina;
    }

    public ArrayList<Equipo> getEquipos() {
        return Equipos;
    }

    public void setEquipos(ArrayList<Equipo> equipos) {
        Equipos = equipos;
    }
}
