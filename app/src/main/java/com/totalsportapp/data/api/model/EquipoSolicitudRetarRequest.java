package com.totalsportapp.data.api.model;

import hirondelle.date4j.DateTime;

public class EquipoSolicitudRetarRequest {

    public int idEquipo;
    public int idEquipoRetador;
    public String comentarios;
    public DateTime fechaReto;
    public String ubigeo;
    public String nombreSede;

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public int getIdEquipoRetador() {
        return idEquipoRetador;
    }

    public void setIdEquipoRetador(int idEquipoRetador) {
        this.idEquipoRetador = idEquipoRetador;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public DateTime getFechaReto() {
        return fechaReto;
    }

    public void setFechaReto(DateTime fechaReto) {
        this.fechaReto = fechaReto;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    public String getNombreSede() {
        return nombreSede;
    }

    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }
}
