package com.totalsportapp.data.api.model;

public class EquipoSolicitudUnirRequest {

    private int idEquipo;
    private String comentariosSolicitud;

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getComentariosSolicitud() {
        return comentariosSolicitud;
    }

    public void setComentariosSolicitud(String comentariosSolicitud) {
        this.comentariosSolicitud = comentariosSolicitud;
    }
}
