package com.totalsportapp.data.api.model;

public class ParticipantModel {

    private int idUsuario;
    private int asistencia;

    public ParticipantModel(int idUsuario, int asistencia) {
        this.idUsuario = idUsuario;
        this.asistencia = asistencia;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(int asistencia) {
        this.asistencia = asistencia;
    }
}
