package com.totalsportapp.data.api.model;

import java.util.ArrayList;

public class CalendarioEquipoOtros {
    private int TotalPagina;
    private ArrayList<CalendarioEquipoAPI> ListCalendarioEquipoOtros;


    public int getTotalPagina() {
        return TotalPagina;
    }

    public void setTotalPagina(int totalPagina) {
        TotalPagina = totalPagina;
    }

    public ArrayList<CalendarioEquipoAPI> getListCalendarioEquipoOtros() {
        return ListCalendarioEquipoOtros;
    }

    public void setListCalendarioEquipoOtros(ArrayList<CalendarioEquipoAPI> listCalendarioEquipoOtros) {
        ListCalendarioEquipoOtros = listCalendarioEquipoOtros;
    }
}
