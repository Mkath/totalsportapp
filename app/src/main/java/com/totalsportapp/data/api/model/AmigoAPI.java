package com.totalsportapp.data.api.model;

public class AmigoAPI {

    private int idUsuarioAmigo;
    private String nombres;
    private String apellidos;
    private String alias;
    private String foto;
    private int estado;
    private boolean agregadoPorMi;
    private int idSolicitud;

    public int getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(int idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getNombreCompleto(){
        return this.nombres + " " + this.getApellidos();
    }

    public int getIdUsuarioAmigo() {
        return idUsuarioAmigo;
    }

    public void setIdUsuarioAmigo(int idUsuarioAmigo) {
        this.idUsuarioAmigo = idUsuarioAmigo;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public boolean isAgregadoPorMi() {
        return agregadoPorMi;
    }

    public void setAgregadoPorMi(boolean agregadoPorMi) {
        this.agregadoPorMi = agregadoPorMi;
    }
}
