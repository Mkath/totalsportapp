package com.totalsportapp.data.db.model;

public class EquipoMiembro {

    private int idEquipo;
    private int idUsuario;
    private int estado;

    public EquipoMiembro() {
    }

    public EquipoMiembro(int idEquipo, int idUsuario, int estado) {
        this.idEquipo = idEquipo;
        this.idUsuario = idUsuario;
        this.estado = estado;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
}
