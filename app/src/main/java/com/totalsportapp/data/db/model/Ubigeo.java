package com.totalsportapp.data.db.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Ubigeo {

    @Id
    private String codigo;
    private String nombre;

    @Generated(hash = 1420893776)
    public Ubigeo(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    @Generated(hash = 1920155935)
    public Ubigeo() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
