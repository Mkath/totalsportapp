package com.totalsportapp.data.db.model;

public class Gender {
    public final static int MAN = 1;
    public final static int WOMAN = 2;
}
