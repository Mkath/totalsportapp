package com.totalsportapp.data.db.model;

public class EstadoAmigo {
    public final static int PENDIENTE = 0;
    public final static int ACEPTADO = 1;
    public final static int DENEGADO = 2;
}
