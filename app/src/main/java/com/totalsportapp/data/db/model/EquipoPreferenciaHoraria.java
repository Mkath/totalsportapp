package com.totalsportapp.data.db.model;

public class EquipoPreferenciaHoraria {

    private int idEquipo;
    private int dia;
    private int hora;

    public EquipoPreferenciaHoraria() {
    }

    public EquipoPreferenciaHoraria(int idEquipo, int dia, int hora) {
        this.idEquipo = idEquipo;
        this.dia = dia;
        this.hora = hora;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }
}
